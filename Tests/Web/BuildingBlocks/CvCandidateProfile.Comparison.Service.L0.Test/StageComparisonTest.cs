﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test
{
    public class StageComparisonTest
    {
        [Fact]
        public void UpdateProfileReviewStages_CandidateId_Is0()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>())).Returns(string.Empty);
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            postgreSqlServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>())).Returns(true);

            // Act Assert
            var ex = Assert.Throws<ValidationException>(() =>
                comparisonService.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<List<Stage>>()));
            Assert.Equal($"Data from request CandidateId=0 is not valid", ex.Message);
        }

        [Fact]
        public void UpdateProfileReviewStages_CvId_Is0()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            var languageFactoryMock = new Mock<LanguageFaсtory>();

            postgreSqlServiceMock.Setup(x => x.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>())).Returns(string.Empty);

            var comparisonService = new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);

            postgreSqlServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>())).Returns(true);

            // Act Assert
            var ex = Assert.Throws<ValidationException>(() =>
                comparisonService.UpdateProfileReviewStages(5, It.IsAny<int>(), It.IsAny<List<Stage>>()));

            Assert.Equal("Data from request DaxtraStoreId=0 is not valid", ex.Message);
        }

        [Fact]
        public void UpdateProfileReviewStages_CandidateIdandCvId_empty()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>())).Returns(string.Empty);
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            postgreSqlServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            var requestField = new List<Stage>()
            {
                new Stage()
                {
                    PropertyName = "Surname",
                    StageStatus = StageStatus.Reviewed
                },
                new Stage()
                {
                    PropertyName = "Forename",
                    StageStatus = StageStatus.Reviewed
                }
            };

            // Act Assert
            Assert.Throws<ValidationException>(() =>
                comparisonService.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<List<Stage>>()));
        }

        [Fact]
        public void UpdateProfileReviewStages_CandidateIdandCvId_NoValidResultField()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>())).Returns(string.Empty);
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            postgreSqlServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            var requestField = new List<Stage>()
            {
                new Stage()
                {
                    PropertyName = "test",
                    StageStatus = StageStatus.Reviewed
                },
                new Stage()
                {
                    PropertyName = "Forename",
                    StageStatus = StageStatus.Reviewed
                }
            };

            var ex = Assert.Throws<ValidationException>(() =>
                comparisonService.UpdateProfileReviewStages(5, 4, It.IsAny<List<Stage>>()));
            // Act Assert
            Assert.Equal("Data from request PropertyStageStatuses is not valid", ex.Message);
        }

        [Fact]
        public void UpdateProfileReviewStages_CandidateIdandCvId_NovalidResultFieldFromPostgre()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>())).Returns("test");
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            postgreSqlServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            var requestField = new List<Stage>()
            {
                new Stage()
                {
                    PropertyName = "test",
                    StageStatus = StageStatus.Reviewed
                },
                new Stage()
                {
                    PropertyName = "Forename",
                    StageStatus = StageStatus.Reviewed
                }
            };

            // Act Assert
            Assert.True(comparisonService.UpdateProfileReviewStages(5, 4, requestField));
        }

        [Fact]
        public void IsCandidateProfileReviewFinishedAsync_HasNotReviewedData_ReturnsFalse()
        {
            // Arrange
            var validationResults = new ValidationResults
            {
                ComparisonStages = new List<Stage>
                {
                    new Stage { PropertyName = "Prop1", StageStatus = StageStatus.NoReviewNeeded },
                    new Stage { PropertyName = "Prop2", StageStatus = StageStatus.Reviewed },
                    new Stage { PropertyName = "Prop3", StageStatus = StageStatus.NotReviewed },
                    new Stage { PropertyName = "Prop4", StageStatus = StageStatus.Reviewed },
                    new Stage { PropertyName = "Prop5", StageStatus = StageStatus.Reviewed },
                },
            };
            var json = JsonConvert.SerializeObject(validationResults);

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.GetCandidateProfileValidationResults(It.IsAny<int>(), null)).Returns(json);
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);

            // Act Assert
            Assert.False(comparisonService.IsCandidateProfileReviewFinished(It.IsAny<int>()));
        }

        [Fact]
        public void IsCandidateProfileReviewFinishedAsync_AllDataReviewed_ReturnsTrue()
        {
            // Arrange
            var validationResults = new ValidationResults
            {
                ComparisonStages = new List<Stage>
                {
                    new Stage { PropertyName = "Prop1", StageStatus = StageStatus.NoReviewNeeded },
                    new Stage { PropertyName = "Prop2", StageStatus = StageStatus.Reviewed },
                    new Stage { PropertyName = "Prop3", StageStatus = StageStatus.Reviewed },
                    new Stage { PropertyName = "Prop4", StageStatus = StageStatus.Reviewed },
                    new Stage { PropertyName = "Prop5", StageStatus = StageStatus.NoReviewNeeded },
                },
            };
            var json = JsonConvert.SerializeObject(validationResults);

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.GetCandidateProfileValidationResults(It.IsAny<int>(), null)).Returns(json);
            var languageFactoryMock = new Mock<LanguageFaсtory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);

            // Act Assert
            Assert.True(comparisonService.IsCandidateProfileReviewFinished(It.IsAny<int>()));
        }
    }
}
