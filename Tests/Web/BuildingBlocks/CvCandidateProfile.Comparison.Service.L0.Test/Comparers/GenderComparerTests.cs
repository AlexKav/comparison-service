﻿using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test.Comparers
{
    public class GenderComparerTests
    {
        [Fact]
        public void GenderComparer_Compare_Equal()
        {
            // Arrange
            var comparer = new GenderComparer();

            // Act
            ComparisonResult result = comparer.Compare(ProfileGender.Female, CvProfileGender.Female);

            // Assert
            Assert.Equal(ComparisonResult.Match, result);
        }

        [Fact]
        public void GenderComparer_Compare_NotEqual()
        {
            // Arrange
            var comparer = new GenderComparer();

            // Act
            ComparisonResult result = comparer.Compare(ProfileGender.Female, CvProfileGender.Male);

            // Assert
            Assert.Equal(ComparisonResult.NotExactMatch, result);
        }
    }
}
