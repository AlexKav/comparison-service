﻿using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test.Comparers
{
    public class ProfileSourcesStatusStringComparerTests
    {
        [Theory]
        [InlineData("Alex", "Fill", ProfileSourcesStatus.ProfileAndDaxtraData)]
        [InlineData(null, "Fill", ProfileSourcesStatus.NoProfileData)]
        [InlineData("", "Fill", ProfileSourcesStatus.NoProfileData)]
        [InlineData("Alex", "", ProfileSourcesStatus.NoDaxtraData)]
        [InlineData("Alex", null, ProfileSourcesStatus.NoDaxtraData)]
        [InlineData(null, null, ProfileSourcesStatus.NoData)]
        [InlineData("", "", ProfileSourcesStatus.NoData)]
        public void Compare_WithValidParams_Equal(string profileFieldDateTo, string jsonFieldContentDateTo, ProfileSourcesStatus profileSourcesStatus)
        {
            // Arrange
            var comparer = new ProfileSourcesStatusStringComparer();

            // Act
            ProfileSourcesStatus result = comparer.Compare(profileFieldDateTo, jsonFieldContentDateTo);

            // Assert
            Assert.Equal(profileSourcesStatus, result);
        }
    }
}
