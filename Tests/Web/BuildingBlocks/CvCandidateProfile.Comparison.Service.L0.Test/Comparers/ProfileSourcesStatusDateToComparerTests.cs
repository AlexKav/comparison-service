﻿using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test.Comparers
{
    public class ProfileSourcesStatusDateToComparerTests
    {
        [Theory]
        [InlineData("2019-01", "2019-01", ProfileSourcesStatus.ProfileAndDaxtraData)]
        [InlineData(null, "2019-01", ProfileSourcesStatus.ProfileAndDaxtraData)]
        [InlineData(null, "Present", ProfileSourcesStatus.ProfileAndDaxtraData)]
        [InlineData("", "2019-01", ProfileSourcesStatus.NoProfileData)]
        [InlineData("", "Present", ProfileSourcesStatus.NoProfileData)]
        [InlineData(null, null, ProfileSourcesStatus.NoDaxtraData)]
        [InlineData("2019-01", null, ProfileSourcesStatus.NoDaxtraData)]
        [InlineData("", "", ProfileSourcesStatus.NoData)]
        [InlineData("", null, ProfileSourcesStatus.NoData)]
        public void Compare_WithValidParams_Equal(string profileFieldDateTo, string jsonFieldContentDateTo, ProfileSourcesStatus profileSourcesStatus)
        {
            // Arrange
            var comparer = new ProfileSourcesStatusDateToComparer();

            // Act
            ProfileSourcesStatus result = comparer.Compare(profileFieldDateTo, jsonFieldContentDateTo);

            // Assert
            Assert.Equal(profileSourcesStatus, result);
        }
    }
}
