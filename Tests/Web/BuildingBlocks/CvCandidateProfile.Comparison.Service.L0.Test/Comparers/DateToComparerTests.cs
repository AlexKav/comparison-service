﻿using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test.Comparers
{
    public class DateToComparerTests
    {
        [Theory]
        [InlineData("2019-01", "2019-01", ComparisonResult.Match)]
        [InlineData(null, "2019-01", ComparisonResult.NotExactMatch)]
        [InlineData(null, "Present", ComparisonResult.Match)]
        [InlineData("", "2019-01", ComparisonResult.NotExactMatch)]
        [InlineData("", "Present", ComparisonResult.NotExactMatch)]
        [InlineData(null, null, ComparisonResult.NotExactMatch)]
        [InlineData("2019-01", null, ComparisonResult.NotExactMatch)]
        [InlineData("", "", ComparisonResult.Match)]
        [InlineData("", null, ComparisonResult.Match)]
        public void Compare_WithValidParams_Equal(string profileFieldDateTo, string jsonFieldContentDateTo, ComparisonResult profileSourcesStatus)
        {
            // Arrange
            var comparer = new DateToComparer();

            // Act
            ComparisonResult result = comparer.Compare(profileFieldDateTo, jsonFieldContentDateTo);

            // Assert
            Assert.Equal(profileSourcesStatus, result);
        }
    }
}
