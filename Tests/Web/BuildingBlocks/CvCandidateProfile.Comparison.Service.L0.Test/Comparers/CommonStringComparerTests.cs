﻿using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test.Comparers
{
    public class CommonStringComparerTests
    {
        [Theory]
        [InlineData("Alex", "Fill", ComparisonResult.NotExactMatch)]
        [InlineData("Alex", "Alex", ComparisonResult.Match)]
        [InlineData(null, "Fill", ComparisonResult.NotExactMatch)]
        [InlineData("", "Fill", ComparisonResult.NotExactMatch)]
        [InlineData("Alex", "", ComparisonResult.NotExactMatch)]
        [InlineData("Alex", null, ComparisonResult.NotExactMatch)]
        [InlineData(null, null, ComparisonResult.Match)]
        [InlineData("", "", ComparisonResult.Match)]
        public void Compare_WithValidParams_Equal(string profileFieldDateTo, string jsonFieldContentDateTo, ComparisonResult profileSourcesStatus)
        {
            // Arrange
            var comparer = new CommonStringComparer();

            // Act
            var result = comparer.Compare(profileFieldDateTo, jsonFieldContentDateTo);

            // Assert
            Assert.Equal(profileSourcesStatus, result);
        }
    }
}
