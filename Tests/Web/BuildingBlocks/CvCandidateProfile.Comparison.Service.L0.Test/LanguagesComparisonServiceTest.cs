﻿using System;
using System.Collections.Generic;
using System.Linq;

using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;
using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Moq;
using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test
{
    public class LanguagesComparisonServiceTest
    {
        private readonly LanguageField _resultLanguageFactoryMatch = new LanguageField
        {
            LanguageId = new ProfileFieldV2
            {
                ProfileFieldName = "LanguageId",
                ProfileFieldContents = "85",
                JsonFieldName = "LanguageId",
                JsonFieldContent = "85"
            },
            Fluency = new ProfileFieldV2
            {
                ProfileFieldName = "Fluency",
                ProfileFieldContents = "Basic",
                JsonFieldName = "Fluency",
                JsonFieldContent = "Basic"
            }
        };

        private readonly LanguageField _resultLanguageFactoryNoMatch = new LanguageField
        {
            LanguageId = new ProfileFieldV2
            {
                ProfileFieldName = "LanguageId",
                ProfileFieldContents = "55",
                JsonFieldName = "LanguageId",
                JsonFieldContent = null
            },
            Fluency = new ProfileFieldV2
            {
                ProfileFieldName = "Fluency",
                ProfileFieldContents = "Basic",
                JsonFieldName = "Fluency",
                JsonFieldContent = null
            }
        };

        private readonly LanguageField _resultLanguageFactoryEmptyProfile = new LanguageField
        {
            LanguageId = new ProfileFieldV2
            {
                ProfileFieldName = "LanguageId",
                ProfileFieldContents = null,
                JsonFieldName = "LanguageId",
                JsonFieldContent = "108"
            },
            Fluency = new ProfileFieldV2
            {
                ProfileFieldName = "Fluency",
                ProfileFieldContents = null,
                JsonFieldName = "Fluency",
                JsonFieldContent = "Basic"
            }
        };

        private static LanguageField ResultFunction(Language inputValue)
        {
            return new LanguageField
            {
                LanguageId = new ProfileFieldV2
                {
                    ProfileFieldName = "LanguageId",
                    ProfileFieldContents = null,
                    JsonFieldName = "LanguageId",
                    JsonFieldContent = "85"
                },
                Fluency = new ProfileFieldV2
                {
                    ProfileFieldName = "Fluency",
                    ProfileFieldContents = null,
                    JsonFieldName = "Fluency",
                    JsonFieldContent = "Basic"
                }
            };
        }

        private static LanguageField ResultFunction2(Language inputValue)
        {
            return new LanguageField();
        }

        [Fact]
        public void CompareProfilesAsync_Language_ProfileAndData()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',                                     
                                      'Languages':[  
                                        {  
                                           'Speak':true,
                                           'Proficiency':'NATIVE',
                                           'LanguageCode':'sq',
                                           'Write':true,
                                           'Read':true
                                        }                                        
                                     ],}}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'Languages':[{
                                                'LanguageId':105,
                                                'Fluency':2
                                                }]}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("sq")).Returns(85);
            var languageFactoryMock = new Mock<ILanguageFactory>();

            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(_resultLanguageFactoryNoMatch);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(_resultLanguageFactoryEmptyProfile);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile())
                .Returns(ResultFunction);
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Act
            var languagesResult = result.Languages.ProfileField;
            // Assert
            Assert.Equal(2, languagesResult.Count());
            Assert.Equal(2, languagesResult.Count(x => x.LanguageId.ComparisonResult == ComparisonResult.NotExactMatch));
        }

        [Fact]
        public void CompareProfilesAsync_Language_ProfileAndData_AnySame()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',                                     
                                      'Languages':[  
                                        {  
                                           'Speak':true,
                                           'Proficiency':'NATIVE',
                                           'LanguageCode':'es',
                                           'Write':true,
                                           'Read':true
                                        }                                      
                                     ],}}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'Languages':[{
                                                'LanguageId':55,
                                                'Fluency':0
                                                }]}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("es")).Returns(55);
            var languageFactoryMock = new Mock<ILanguageFactory>();

            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(_resultLanguageFactoryMatch);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(_resultLanguageFactoryEmptyProfile);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile());
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Act
            var languagesResult = result.Languages.ProfileField;
            // Assert
            var languageFields = languagesResult as LanguageField[] ?? languagesResult.ToArray();
            Assert.Equal(2, languageFields.Length);
            Assert.Equal(1, languageFields.Count(x => x.LanguageId.ComparisonResult == ComparisonResult.Match));
            Assert.Equal(1, languageFields.Count(x => x.Fluency.ComparisonResult == ComparisonResult.Match));
        }

        [Fact]
        public void CompareProfilesAsync_Language_ProfileLanguagesNoFound()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',                                     
                                      'Languages':[  
                                        {  
                                           'Speak':true,
                                           'Proficiency':'NATIVE',
                                           'LanguageCode':'es',
                                           'Write':true,
                                           'Read':true
                                        }
                                     ],}}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                           }}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("sq")).Returns(85);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("es")).Returns(55);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("en")).Returns(108);
            var languageFactoryMock = new Mock<ILanguageFactory>();

            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(new LanguageField());
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(new LanguageField());
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile()).Returns(ResultFunction);
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Act
            var languagesResult = result.Languages.ProfileField;
            // Assert
            var languageFields = languagesResult as LanguageField[] ?? languagesResult.ToArray();
            Assert.Single(languageFields);
            Assert.Single(languageFields.Where(x => x.LanguageId.ComparedFieldsFound == ProfileSourcesStatus.NoProfileData));
            Assert.Single(languageFields.Where(x => x.Fluency.ComparedFieldsFound == ProfileSourcesStatus.NoProfileData));
        }

        [Fact]
        public void CompareProfilesAsync_Language_CvLanguagesNoFound()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester'                                     
                                     }}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'Languages':[{
                                                'LanguageId':55,
                                                'Fluency':0
                                                }]}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("en")).Returns(108);
            var languageFactoryMock = new Mock<ILanguageFactory>();

            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(_resultLanguageFactoryNoMatch);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(_resultLanguageFactoryNoMatch);
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile()).Returns(ResultFunction2);
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);

            // Act
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;
            var languagesResult = result.Languages.ProfileField;
            // Assert
            var languageFields = languagesResult as LanguageField[] ?? languagesResult.ToArray();
            Assert.Equal(1, languageFields?.Count());
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, languageFields.FirstOrDefault()?.LanguageId?.ComparedFieldsFound);
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, languageFields.FirstOrDefault()?.Fluency?.ComparedFieldsFound);
            Assert.Equal("55", languageFields.FirstOrDefault()?.LanguageId.ProfileFieldContents);
            Assert.Null(languageFields.FirstOrDefault()?.LanguageId.JsonFieldContent);
        }

        [Fact]
        public void CompareProfilesAsync_Language_CvAndProfileLanguagesNoFound()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester'                                     
                                     }}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null
                                        }}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("sq")).Returns(85);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("es")).Returns(55);
            postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("en")).Returns(108);
            var languageFactoryMock = new Mock<ILanguageFactory>();

            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(new LanguageField());
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(new LanguageField());
            languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile()).Returns(ResultFunction2);
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactoryMock.Object);
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Act
            var languagesResult = result.Languages.ProfileField;
            // Assert
            Assert.Equal(0, languagesResult.Count);
        }
    }
}
