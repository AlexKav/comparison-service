﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Moq;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test
{
    public class ComparisonServiceTests
    {
        [Fact]
        public async Task CompareProfilesAsync_CvProfileByIdNotFound_ThrowsExceptionAsync()
        {
            // Arrange
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(string.Empty);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            // Act Assert
            await Assert.ThrowsAsync<ValidationException>(async () =>
                await comparisonService.CompareProfilesAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task CompareProfilesAsync_ProfileByIdNotFound_ThrowsExceptionAsync()
        {
            // Arrange
            var profileJson = @"{'Profile': {
                                'UserId': 22229961,
                                'Forename': 'James',
                                'Surname': 'Blond',
                                'ProfileImage': null,
                                'PhotoUploadedOn': '/Date(-62135596800000)/',
                                'IsEmpty': false,
                                'IsComplete': false},
                                'Error': 0}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(string.Empty);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            // Act Assert
            await Assert.ThrowsAsync<ValidationException>(async () =>
                await comparisonService.CompareProfilesAsync(It.IsAny<int>()));
        }

        [Fact]
        public void CompareProfilesAsync_NameSurnameExists_Match_ProfileAndDaxtraData()
        {
            // Arrange
            var profileJson = @"{'Profile': {
                                'UserId': 22229961,
                                'Forename': 'James',
                                'Surname': 'Blond',
                                'ProfileImage': null,
                                'PhotoUploadedOn': '/Date(-62135596800000)/',
                                'IsEmpty': false,
                                'IsComplete': false},
                                'Error': 0}";
            var daxtraJson = @"{'Resume' : {
                                'StructuredResume' : {
                                'ExecutiveSummary' : 'Personal statement.',
                                'PersonName' : {
                                'FormattedName' : 'James Blond',
                                'FamilyName' :'Blond',
                                'GivenName' : 'James',
                                'sex' : 'Male'
                                }}}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            // Act
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;
            var forenameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("forename", StringComparison.InvariantCulture));
            var surnameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("surname", StringComparison.InvariantCulture));

            // Assert
            Assert.NotNull(forenameResult);
            Assert.NotNull(surnameResult);
            Assert.Equal(ComparisonResult.Match, forenameResult.ComparisonResult);
            Assert.Equal(ComparisonResult.Match, surnameResult.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_NameSurnameExists_NoMatch_ProfileAndDaxtraData()
        {
            // Arrange
            var profileJson = @"{'Profile': {
                                'UserId': 22229961,
                                'Forename': 'James',
                                'Surname': 'Blond',
                                'ProfileImage': null,
                                'PhotoUploadedOn': '/Date(-62135596800000)/',
                                'IsEmpty': false,
                                'IsComplete': false},
                                'Error': 0}";
            var daxtraJson = @"{'Resume' : {
                                'StructuredResume' : {
                                'ExecutiveSummary' : 'Personal statement.',
                                'PersonName' : {
                                'FormattedName' : 'Deny Devito',
                                'FamilyName' :'Deny',
                                'GivenName' : 'Devito',
                                'sex' : 'Male'
                                }}}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);

            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            // Act
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;
            var forenameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("forename", StringComparison.InvariantCulture));
            var surnameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("surname", StringComparison.InvariantCulture));

            // Assert
            Assert.NotNull(forenameResult);
            Assert.NotNull(surnameResult);
            Assert.Equal(ComparisonResult.NotExactMatch, forenameResult.ComparisonResult);
            Assert.Equal(ComparisonResult.NotExactMatch, surnameResult.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_OneProfileJobTitleOneDaxtraJobTitle_Match_ProfileAndDaxtraData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }
                                     ]}}}";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':'software tester',
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));

            Assert.Equal(ComparisonResult.Match, jobTitleResult?.ComparisonResult);
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, jobTitleResult?.ComparedFieldsFound);
        }

        [Fact]
        public void CompareProfilesAsync_OneProfileJobTitleOneDaxtraJobTitle_NoMatch_ProfileAndDaxtraData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }
                                     ]}}}";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':'tester',
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_OneProfileJobTitleOneDaxtraJobTitle_NoMatch_NoDaxtraData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2012',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }
                                     ]}}}";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':'tester',
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_NoProfileJobTitleOneDaxtraJobTitle_NoMatch_NoProfileData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }
                                     ]}}}";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            Assert.Equal(ProfileSourcesStatus.NoProfileData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_OneProfileJobTitleTwoDaxtraJobTitle_NoMatch_ProfileAndDaxtraData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                          'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'Developer'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }
                                     ]}}}";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':'developer',
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_NoProfileJobTitleOneDaxtraJobTitle_Match_NoData()
        {
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                       'EmploymentHistory': [{
                                                                'employerOrgType' : 'soleEmployer',
                                                                'EndDate' : '2012',
                                                               'MonthsOfWork' : 60,
                                                                'LocationSummary' : {
                                                                    'Municipality' : 'London',
                                                                    'CountryCode' : 'UK'
                                                                },
                                                                'EmployerOrgName' : 'Organisation LTD',
                                                                'OrgName' : 'Organisation LTD',
                                                                'PositionType' : 'PERMANENT',
                                                                'StartDate' : '2007',
                                                                'Description' : 'The details have been placed into the Details box to see if they continue out of the field.'
                                                            }],}}
                                                }";

            var profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'Billie',
                                            'Surname':'Pacocha',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'\/Date(-62135596800000)\/',
                                            'Email':'constance@greenholt.ca',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'NatalliaTarhonia_(2).doc',
                                            'PersonalStatement':null}}";
            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            Assert.Equal(ProfileSourcesStatus.NoData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.Match, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmptyProfile_MatchNoData()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                       'EmploymentHistory': [{
                                                                'employerOrgType' : 'soleEmployer',
                                                                'EndDate' : '2012',
                                                               'MonthsOfWork' : 60,
                                                                'LocationSummary' : {
                                                                    'Municipality' : 'London',
                                                                    'CountryCode' : 'UK'
                                                                },
                                                                'EmployerOrgName' : 'Organisation LTD',
                                                                'OrgName' : 'Organisation LTD',
                                                                'PositionType' : 'PERMANENT',
                                                                'StartDate' : '2007',
                                                                'Description' : 'The details have been placed into the Details box to see if they continue out of the field.'
                                                            }],}}
                                                }";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var forenameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("forename", StringComparison.InvariantCulture));
            var surnameResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("surname", StringComparison.InvariantCulture));
            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            var personalStatementResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("personalstatement", StringComparison.InvariantCulture));
            var isGraduateResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("isgraduate", StringComparison.InvariantCulture));

            // Assert
            Assert.Equal(ProfileSourcesStatus.NoProfileData, forenameResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, forenameResult?.ComparisonResult);

            Assert.Equal(ProfileSourcesStatus.NoProfileData, surnameResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, surnameResult?.ComparisonResult);

            Assert.Equal(ProfileSourcesStatus.NoData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.Match, jobTitleResult?.ComparisonResult);

            Assert.Equal(ProfileSourcesStatus.NoProfileData, personalStatementResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, personalStatementResult?.ComparisonResult);

            Assert.Equal(ProfileSourcesStatus.NoData, isGraduateResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.Match, isGraduateResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmploymentHistoryEndDate_Null_NoDaxtraData()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                       'EmploymentHistory': [{
                                                                'employerOrgType' : 'soleEmployer',
                                                                'EndDate' : '',
                                                                'MonthsOfWork' : 60,
                                                                'LocationSummary' : {
                                                                    'Municipality' : 'London',
                                                                    'CountryCode' : 'UK'
                                                                },
                                                                'EmployerOrgName' : 'Organisation LTD',
                                                                'OrgName' : 'Organisation LTD',
                                                                'PositionType' : 'PERMANENT',
                                                                'StartDate' : '2007',
                                                                'Description' : 'The details have been placed into the Details box to see if they continue out of the field.'
                                                            }],}}
                                                }";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            // Assert
            Assert.Equal(ProfileSourcesStatus.NoData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.Match, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmploymentHistoryEndDate_present_NoDaxtraData()
        {
            // Arrange
            const string daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Tester',
                                       'EmploymentHistory': [{
                                                                'employerOrgType' : 'soleEmployer',
                                                                'EndDate' : 'present',
                                                                'MonthsOfWork' : 60,
                                                                'LocationSummary' : {
                                                                    'Municipality' : 'London',
                                                                    'CountryCode' : 'UK'
                                                                },
                                                                'EmployerOrgName' : 'Organisation LTD',
                                                                'OrgName' : 'Organisation LTD',
                                                                'PositionType' : 'PERMANENT',
                                                                'StartDate' : '2007',
                                                                'Description' : 'The details have been placed into the Details box to see if they continue out of the field.'
                                                            },{
                                                                'employerOrgType' : 'soleEmployer',
                                                                'EndDate' : 'present',
                                                                'MonthsOfWork' : 60,
                                                                'LocationSummary' : {
                                                                    'Municipality' : 'London',
                                                                    'CountryCode' : 'UK'
                                                                },
                                                                'EmployerOrgName' : 'Organisation LTD',
                                                                'OrgName' : 'Organisation LTD',
                                                                'PositionType' : 'PERMANENT',
                                                                'StartDate' : '2007',
                                                                'Description' : 'The details have been placed into the Details box to see if they continue out of the field.'
                                                            }],}}
                                                }";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>()))
                .ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);

            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var jobTitleResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("jobtitle", StringComparison.InvariantCulture));
            // Assert
            Assert.Equal(ProfileSourcesStatus.NoData, jobTitleResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.Match, jobTitleResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmptyDegreeType_MatchNoData()
        {
            // Arrange
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                    'EducationHistory':[  
                                                        {  
                                                           'EndDate':'2014-02',
                                                           'Comments':'Currículum vítae',
                                                           'LocationSummary':{  
                                                              'Municipality':'Dresden',
                                                              'CountryCode':'DE'
                                                           },
                                                           'Major':'Informatics',
                                                           'schoolType':'university',
                                                           'Degree':{  
                                                              'degreeType':null,
                                                              'DegreeDate':'2014-02'
                                                           },
                                                           'StartDate':'2013-09',
                                                           'SchoolName':'Technische Universität Dresden'
                                                        }            
                                                     ],}}
                                                }";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'IsGraduate':0}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>())).ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var isGraduateResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("isgraduate", StringComparison.InvariantCulture));

            // Assert
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, isGraduateResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, isGraduateResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmptyDegree_MatchNoData()
        {
            // Arrange
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                    'EducationHistory':[  
                                                        {  
                                                           'EndDate':'2014-02',
                                                           'Comments':'Currículum vítae',
                                                           'LocationSummary':{  
                                                              'Municipality':'Dresden',
                                                              'CountryCode':'DE'
                                                           },
                                                           'Major':'Informatics',
                                                           'schoolType':'university',
                                                           'Degree':null,
                                                           'StartDate':'2013-09',
                                                           'SchoolName':'Technische Universität Dresden'
                                                        }            
                                                     ],}}
                                                }";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'IsGraduate':0}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>())).ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var isGraduateResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("isgraduate", StringComparison.InvariantCulture));
            // Assert
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, isGraduateResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, isGraduateResult?.ComparisonResult);
        }

        [Fact]
        public void CompareProfilesAsync_EmptyEducationHistory_MatchNoData()
        {
            // Arrange
            var daxtraJson = @"{'Resume' : {
                                  'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Bloggs',
                                         'FamilyName' : 'Bloggs',
                                         'GivenName' : 'Joe',
                                        'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                    }}}";

            const string profileJson = @"{'Profile':{'UserId':22229961,
                                            'Title':null,
                                            'Forename':'',
                                            'Surname':'',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'IsGraduate':0}}";

            var postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            var candidateProfileServiceMock = new Mock<ICandidateProfileService>();

            postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);
            candidateProfileServiceMock.Setup(x => x.GetJsonCandidateProfileAsync(It.IsAny<int>())).ReturnsAsync(profileJson);
            var languageFactory = new Mock<ILanguageFactory>();
            var comparisonService =
                new ComparisonService(postgreSqlServiceMock.Object, candidateProfileServiceMock.Object, languageFactory.Object);
            var result = comparisonService.CompareProfilesAsync(It.IsAny<int>()).Result;

            // Act
            var isGraduateResult = result.ProfileFields.FirstOrDefault(f =>
                f.ProfileFieldName.ToLower().Equals("isgraduate", StringComparison.InvariantCulture));
            // Assert
            Assert.Equal(ProfileSourcesStatus.NoDaxtraData, isGraduateResult?.ComparedFieldsFound);
            Assert.Equal(ComparisonResult.NotExactMatch, isGraduateResult?.ComparisonResult);
        }
    }
}
