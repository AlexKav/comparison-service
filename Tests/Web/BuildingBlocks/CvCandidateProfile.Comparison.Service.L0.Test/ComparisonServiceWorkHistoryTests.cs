﻿using System.Collections.Generic;
using System.Linq;

using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;
using CvCandidateProfile.PostgreSql.Service.Interfaces;

using Moq;

using Newtonsoft.Json;

using Xunit;

namespace CvCandidateProfile.Comparison.Service.L0.Test
{
    public class ComparisonServiceWorkHistoryTests
    {
        private readonly Mock<ICandidateProfileService> _candidateProfileServiceMock;
        private readonly Mock<IPostgreSqlService> _postgreSqlServiceMock;
        private readonly Mock<ILanguageFactory> _languageFactoryMock;

        private static readonly IReadOnlyCollection<ComparisonTestCase> ComparisonTestCases = new[]
        {
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2006-06-01T00:00:00',
                                                   'EndDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                 },
                                                 {
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2003-01-01T00:00:00',
                                                   'EndDate' : '2006-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2006-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2003-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "Present"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2006-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2008-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2003-01",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2003-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2006-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "Present"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'EndDate' : null,
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2006-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2003-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "Present"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2006-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2008-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2003-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2006-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus()
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'EndDate' : null,
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'EndDate' : '2008-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2006-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                 },
                                                 {
                                                   'EndDate' : '2006-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2003-01-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = null
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldContent = null,
                                    JsonFieldName = "OrgName"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldContent = "2006-06",
                                    JsonFieldName = "StartDate"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2008-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2003-01",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = null
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus()
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2006-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2003-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldContent = "Present",
                                    JsonFieldName = "EndDate"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2006-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2008-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2003-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2006-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus()
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'EndDate' : null,
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'EndDate' : '2008-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2006-06',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                 },
                                                 {
                                                   'EndDate' : '2006-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2003-01-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste '
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = null
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = null
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = null
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2003-01",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = null
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'EndDate' : null,
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'EndDate' : '2008-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2006-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                 },
                                                 {
                                                   'EndDate' : '2006-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2003-01-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'EndDate' : '2003-01-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2000-01',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2006-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2003-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "Present"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldContent = "SOFTWARE TESTER",
                                    JsonFieldName = "Title"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    JsonFieldContent = "2006-06",
                                    JsonFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    ProfileFieldName = "StartDate"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    JsonFieldContent = "2008-06",
                                    JsonFieldName = "EndDate",
                                    ProfileFieldContents = "2008-06",
                                    ProfileFieldName = "EndDate",
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2003-01",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2003-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2006-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2003-01",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = null
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = null
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = null
                                },
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2000-01",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = null
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'EndDate' : null,
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                },
                                                {
                                                   'EndDate' : '2008-06-01T00:00:00',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'Company' : 'Software Consultancy Company',
                                                   'StartDate' : '2006-06-01T00:00:00',
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                 }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2003-01',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2001-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2006-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2003-01',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2008-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2006-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        },
                                        {
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'Present',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'EmployerOrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'OrgName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "Present"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2006-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2008-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2003-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2006-06"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            },
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2001-01"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2003-01"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste '
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = null
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new List<WorkHistory>()
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'PRESENT',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "PRESENT"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'PRESENT',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = "SOFTWARE TESTER TESTER"
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2008-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = "2008-06"
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "PRESENT"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            },
            new ComparisonTestCase
            {
                ProfileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2006-06-01T00:00:00',
                                                   'EndDate' : null,
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}",
                DaxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2019-05',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                           'Title' : [
                                              'SOFTWARE TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : null,
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}",
                ExpectedComparisonResult = new CvProfileResponseModelV2
                {
                    Forename = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Forename",
                            ProfileFieldContents = "Joe",
                            JsonFieldName = "GivenName",
                            JsonFieldContent = "Joe"
                        }
                    },
                    Surname = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "Surname",
                            ProfileFieldContents = "Blogs",
                            JsonFieldName = "FamilyName",
                            JsonFieldContent = "Blogs"
                        }
                    },
                    Title = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "JobTitle",
                            ProfileFieldContents = null,
                            JsonFieldName = "JobTitle",
                            JsonFieldContent = null
                        }
                    },
                    PersonalStatement = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "PersonalStatement",
                            ProfileFieldContents = null,
                            JsonFieldName = "PersonalStatement",
                            JsonFieldContent = "Currently working as part of a close knit team performing "
                        }
                    },
                    IsGraduate = new ProfileFieldStatus
                    {
                        ProfileField = new ProfileFieldV2
                        {
                            ProfileFieldName = "IsGraduate",
                            ProfileFieldContents = null,
                            JsonFieldName = "IsGraduate",
                            JsonFieldContent = null
                        }
                    },
                    UserWorkHistory = new WorkHistoryStatus
                    {
                        ProfileField = new[]
                        {
                            new WorkHistory
                            {
                                DateFrom = new ProfileFieldV2
                                {
                                    ProfileFieldName = "StartDate",
                                    ProfileFieldContents = "2006-06",
                                    JsonFieldName = "StartDate",
                                    JsonFieldContent = null
                                },
                                DateTo = new ProfileFieldV2
                                {
                                    ProfileFieldName = "EndDate",
                                    ProfileFieldContents = null,
                                    JsonFieldName = "EndDate",
                                    JsonFieldContent = "2019-05"
                                },
                                CompanyName = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Company",
                                    ProfileFieldContents = "Software Consultancy Company",
                                    JsonFieldName = "OrgName",
                                    JsonFieldContent = "Software Consultancy Company"
                                },
                                JobTitle = new ProfileFieldV2
                                {
                                    ProfileFieldName = "JobTitle",
                                    ProfileFieldContents = "SOFTWARE TESTER",
                                    JsonFieldName = "Title",
                                    JsonFieldContent = "SOFTWARE TESTER"
                                },
                                JobDescription = new ProfileFieldV2
                                {
                                    ProfileFieldName = "Responsibilities",
                                    ProfileFieldContents = "Liaising closely with the software development team to understand what a software programmer",
                                    JsonFieldName = "Description",
                                    JsonFieldContent = "Liaising closely with the software development team to understand what a software programmer"
                                }
                            }
                        }
                    },
                    Languages = new LanguageStatus
                    {
                        ProfileField = new List<LanguageField>()
                    }
                }
            }
        };

        public ComparisonServiceWorkHistoryTests()
        {
            _candidateProfileServiceMock = new Mock<ICandidateProfileService>();
            _postgreSqlServiceMock = new Mock<IPostgreSqlService>();
            _languageFactoryMock = new Mock<ILanguageFactory>();

            _postgreSqlServiceMock.Setup(x => x.GetLanguageIdByLanguageCode("es")).Returns(55);

            _languageFactoryMock.Setup(x =>
                    x.CreateLanguageFieldFromProfile(It.IsAny<IEnumerable<Language>>(), It.IsAny<Language>()))
                .Returns(new LanguageField());
            _languageFactoryMock.Setup(x =>
                    x.CreateLanguageFieldFromCv(It.IsAny<Language>(), It.IsAny<IEnumerable<LanguageField>>()))
                .Returns(new LanguageField());
            _languageFactoryMock.Setup(x => x.CreateLanguageFieldFromCvWithEmptyProfile()).Returns(inputValue => new LanguageField());
        }

        [Fact]
        public void CompareProfilesAsync_WithValidEmploymentHistoryParams_MatchWithExpectedResult()
        {
            foreach (ComparisonTestCase comparisonTestCase in ComparisonTestCases)
            {
                // Arrange
                _postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(comparisonTestCase.DaxtraJson);

                var comparisonService = new ComparisonService(_postgreSqlServiceMock.Object, _candidateProfileServiceMock.Object, _languageFactoryMock.Object);

                // Act
                var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), comparisonTestCase.ProfileJson).Result;

                var dee = JsonConvert.SerializeObject(comparisonTestCase.ExpectedComparisonResult);
                var dew = JsonConvert.SerializeObject(result);

                // Assert
                Assert.Equal(JsonConvert.SerializeObject(comparisonTestCase.ExpectedComparisonResult), JsonConvert.SerializeObject(result));
            }
        }

        [Fact]
        public void CompareProfilesAsync_WithValidEmploymentHistoryParamsWithResults_MatchWithComparisonResult()
        {
            var profileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}";
            var daxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : 'PRESENT',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}";

            // Arrange
            _postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);

            var comparisonService = new ComparisonService(_postgreSqlServiceMock.Object, _candidateProfileServiceMock.Object, _languageFactoryMock.Object);

            // Act
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Assert
            Assert.Equal(ComparisonResult.Match, result.UserWorkHistory.ProfileField.Single().DateTo.ComparisonResult);
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, result.UserWorkHistory.ProfileField.Single().DateTo.ComparedFieldsFound);
        }

        [Fact]
        public void CompareProfilesAsync_WithNotExactMatchEmploymentHistoryParams_MatchWithComparisonResult()
        {
            var profileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : '2010-06',                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}";
            var daxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2012-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}";

            // Arrange
            _postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);

            var comparisonService = new ComparisonService(_postgreSqlServiceMock.Object, _candidateProfileServiceMock.Object, _languageFactoryMock.Object);

            // Act
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Assert
            Assert.Equal(ComparisonResult.NotExactMatch, result.UserWorkHistory.ProfileField.Single().DateTo.ComparisonResult);
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, result.UserWorkHistory.ProfileField.Single().DateTo.ComparedFieldsFound);
        }

        [Fact]
        public void CompareProfilesAsync_WithEndDateNull_MatchWithComparisonResult()
        {
            var profileJson = @"{'Profile':{'UserId':99999999,
                                            'Title':null,
                                            'Forename':'Joe',
                                            'Surname':'Blogs',
                                            'ProfileImage':null,
                                            'PhotoUploadedOn':'',
                                            'Email':'',
                                            'Postcode':null,
                                            'Country':null,
                                            'Town':null,
                                            'CoverLetter':null,
                                            'CvName':'',
                                            'PersonalStatement':null,
                                            'WorkExperience' : [{
                                                   'Company' : 'Software Consultancy Company',
                                                   'JobTitle' : 'SOFTWARE TESTER',
                                                   'StartDate' : '2008-06-01T00:00:00',
                                                   'EndDate' : null,                                                   
                                                   'Responsibilities' : 'Liaising closely with the software development team to understand what a software programmer'
                                                }]
                                }}";
            var daxtraJson = @"{'Resume' : {
                                    'StructuredResume' : {
                                            'ContactMethod' : {
                                                'InternetEmailAddress_main' :'joe.bloggs@tester.com',
                                        },
                                     'ExecutiveSummary' : 'Currently working as part of a close knit team performing ',
                                     'PersonName' : {
                                         'FormattedName' : 'Joe Blogs',
                                         'FamilyName' : 'Blogs',
                                         'GivenName' : 'Joe',
                                         'sex' : 'Male'
                                     },
                                     'RevisionDate' : '2014-12-01',
                                     'lang' : 'en',
                                     'PreferredPosition' : 'Software Teste ',
                                     'EmploymentHistory' : [{
                                           'employerOrgType' : 'soleEmployer',
                                           'EndDate' : '2012-06',
                                           'MonthsOfWork' : 79,
                                           'LocationSummary' : {
                                              'Municipality' : 'Coventry',
                                              'CountryCode' : 'UK'
                                           },
                                           'OrgName' : 'Software Consultancy Company',
                                            'Title' : [
                                              'SOFTWARE TESTER TESTER'
                                           ],
                                           'CompanyName' : 'Software Consultancy Company',
                                           'PositionType' : 'PERMANENT',
                                           'JobArea' : 'it',
                                           'JobGrade' : 'JobGrade',
                                           'StartDate' : '2008-06',
                                           'Description' : 'Liaising closely with the software development team to understand what a software programmer'
                                        }]
                                }}}";

            // Arrange
            _postgreSqlServiceMock.Setup(x => x.ReadDaxtraByIdAsync(It.IsAny<int>())).ReturnsAsync(daxtraJson);

            var comparisonService = new ComparisonService(_postgreSqlServiceMock.Object, _candidateProfileServiceMock.Object, _languageFactoryMock.Object);

            // Act
            var result = comparisonService.CompareProfilesV2Async(It.IsAny<int>(), profileJson).Result;

            // Assert
            Assert.Equal(ComparisonResult.NotExactMatch, result.UserWorkHistory.ProfileField.Single().DateTo.ComparisonResult);
            Assert.Equal(ProfileSourcesStatus.ProfileAndDaxtraData, result.UserWorkHistory.ProfileField.Single().DateTo.ComparedFieldsFound);
        }

        private class ComparisonTestCase
        {
            public string ProfileJson { get; set; }

            public string DaxtraJson { get; set; }

            public CvProfileResponseModelV2 ExpectedComparisonResult { get; set; }
        }
    }
}
