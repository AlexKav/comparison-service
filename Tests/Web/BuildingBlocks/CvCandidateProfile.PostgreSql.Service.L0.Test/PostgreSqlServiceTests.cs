﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using CvCandidateProfile.PostgreSql.Service.Interfaces;
using CvCandidateProfile.PostgreSql.Service.Models;
using Moq;
using Npgsql;
using Xunit;

namespace CvCandidateProfile.PostgreSql.Service.L0.Test
{
    public class PostgreSqlServiceTests
    {
        [Fact]
        public void ReadDaxtraByIdAsyncTest_WithValidInput_Succeeds()
        {
            // Arrange
            const string mockResult = "mock result";

            var dbCommandMock = new Mock<IDbCommand>();
            var dbConnection = new Mock<IDbConnection>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(mockResult);
            dbConnection.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            factory.Setup(c => c.CreateConnection()).Returns(dbConnection.Object);

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act
            var expectedDaxtra = postgreSqlService.ReadDaxtraByIdAsync(It.IsAny<int>());

            // Assert
            Assert.Equal(mockResult, expectedDaxtra.Result);
        }

        [Fact]
        public async Task ReadDaxtraByIdAsyncTest_WithInValidInput_ThrowsException()
        {
            // Arrange
            const string mockResult = "";

            var dbCommandMock = new Mock<IDbCommand>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(mockResult);

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act Assert
            await Assert.ThrowsAsync<PostgreSqlClientException>(async () =>
                await postgreSqlService.ReadDaxtraByIdAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task ReadDaxtraByIdAsyncTest_WithException_ThrowsException()
        {
            // Arrange
            var dbCommandMock = new Mock<IDbCommand>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Throws<PostgresException>();

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act Assert
            await Assert.ThrowsAsync<PostgreSqlClientException>(async () =>
                await postgreSqlService.ReadDaxtraByIdAsync(It.IsAny<int>()));
        }

        [Fact]
        public void ExecuteQuery_WithValidInput_Succeeds()
        {
            // Arrange
            const string mockResult = "mock result";
            var dbCommandMock = new Mock<IDbCommand>();
            var dbConnection = new Mock<IDbConnection>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(mockResult);
            dbConnection.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            factory.Setup(c => c.CreateConnection()).Returns(dbConnection.Object);
            var postgreSqlService = new PostgreSqlService(factory.Object);
            // Act
            var result = postgreSqlService.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>());

            // Assert
            Assert.Equal(mockResult, result);
        }

        [Fact]
        public void ExecuteQuery_WithValidInput_ReturnsNull()
        {
            // Arrange
            const string mockResult = null;

            var dbCommandMock = new Mock<IDbCommand>();
            var dbConnection = new Mock<IDbConnection>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(mockResult);
            dbConnection.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            factory.Setup(c => c.CreateConnection()).Returns(dbConnection.Object);

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act
            var result = postgreSqlService.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>());

            // Assert
            Assert.Equal(mockResult, result);
        }

        [Fact]
        public void ExecuteQuery_WithException_ThrowsValidTypeException()
        {
            // Arrange
            const string mockResult = "mock result";

            var dbCommandMock = new Mock<IDbCommand>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(mockResult);

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act
            // Assert
            Assert.Throws<PostgreSqlClientException>(() => postgreSqlService.ExecuteQuery(It.IsAny<string>(), It.IsAny<int>()));
        }

        [Fact]
        public void Ctor_WithoutConnection_ThrowsValidTypeException()
        {
            // Arrange
            // Act
            // Assert
            try
            {
                var service = new PostgreSqlService(null);
            }
            catch (Exception exc)
            {
                Assert.Equal(typeof(ArgumentNullException), exc.GetType());
            }
        }

        [Fact]
        public void UpdateProfileReviewStages_WithValidInput_Succeeds()
        {
            var dbFactoryMock = new Mock<IDbConnectionFactory>();
            var dbConnectionMock = new Mock<IDbConnection>();
            var dbCommandMock = new Mock<IDbCommand>();
            var parameters = new Mock<IDataParameterCollection>();

            dbCommandMock.Setup(p => p.Parameters).Returns(parameters.Object);

            dbConnectionMock.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            dbFactoryMock.Setup(c => c.CreateConnection()).Returns(dbConnectionMock.Object);
            var postgreSqlService = new PostgreSqlService(dbFactoryMock.Object);
            dbCommandMock.Setup(c => c.ExecuteNonQuery()).Returns(-1);
            // Act
            var result = postgreSqlService.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<string>());

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void GetCandidateProfileValidationResults_WithException_ThrowsException()
        {
            // Arrange
            var dbCommandMock = new Mock<IDbCommand>();
            var factory = new Mock<IDbConnectionFactory>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Throws<PostgresException>();

            var postgreSqlService = new PostgreSqlService(factory.Object);

            // Act Assert
            Assert.Throws<PostgreSqlClientException>(() => postgreSqlService.GetCandidateProfileValidationResults(It.IsAny<int>()));
        }

        [Fact]
        public void GetLanguageIdByLanguageCode_Succeeds()
        {
            var dbFactoryMock = new Mock<IDbConnectionFactory>();
            var dbConnectionMock = new Mock<IDbConnection>();
            var dbCommandMock = new Mock<IDbCommand>();
            var parameters = new Mock<IDataParameterCollection>();

            dbCommandMock.Setup(p => p.Parameters).Returns(parameters.Object);

            dbConnectionMock.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            dbFactoryMock.Setup(c => c.CreateConnection()).Returns(dbConnectionMock.Object);
            var postgreSqlService = new PostgreSqlService(dbFactoryMock.Object);
            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(5);
            // Act
            var result = postgreSqlService.GetLanguageIdByLanguageCode(It.IsAny<string>());
            Assert.Equal(5, result);
        }

        [Fact]
        public void GetLanguageIdByLanguageCode_Exception()
        {
            var dbFactoryMock = new Mock<IDbConnectionFactory>();
            var dbConnectionMock = new Mock<IDbConnection>();
            var dbCommandMock = new Mock<IDbCommand>();
            var parameters = new Mock<IDataParameterCollection>();

            dbCommandMock.Setup(p => p.Parameters).Returns(parameters.Object);

            dbConnectionMock.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);
            dbFactoryMock.Setup(c => c.CreateConnection()).Returns(dbConnectionMock.Object);
            var postgreSqlService = new PostgreSqlService(dbFactoryMock.Object);
            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns("test");
            // Act
            Assert.Throws<PostgreSqlClientException>(() => postgreSqlService.GetLanguageIdByLanguageCode(It.IsAny<string>()));
        }
    }
}
