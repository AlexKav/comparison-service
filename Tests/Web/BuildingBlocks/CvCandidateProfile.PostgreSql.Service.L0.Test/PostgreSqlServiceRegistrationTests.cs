﻿using System;

using CvCandidateProfile.PostgreSql.Service.Models;

using Microsoft.Extensions.DependencyInjection;

using Xunit;

namespace CvCandidateProfile.PostgreSql.Service.L0.Test
{
    public class PostgreSqlServiceRegistrationTests
    {
        [Fact]
        public void AddPostgreSqlServiceTest_WithValidInput_Succeeds()
        {
            // Arrange
            var serviceCollectionMock = new ServiceCollection();

            PostgreSqlSettings postgreSqlSettings = new PostgreSqlSettings
            {
                ConnectionString = "Server=mockpsql; Port=5432; Database=mockDb; User Id=mockUserId; Password=mockPassword; Timeout=300"
            };

            var postgreSqlSettingsMock = new Action<PostgreSqlSettings>(c =>
            {
            });

            // Act
            serviceCollectionMock.AddPostgreSqlService(
                postgreSqlSettingsMock,
                postgreSqlSettings.Value.ConnectionString);

            // Assert
            Assert.True(serviceCollectionMock.Count > 0);
        }
    }
}
