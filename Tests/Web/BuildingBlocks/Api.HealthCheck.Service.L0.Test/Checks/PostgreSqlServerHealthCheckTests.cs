﻿using System;
using System.Data;
using System.Threading.Tasks;

using Api.HealthCheck.Service.Checks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Moq;
using Xunit;

namespace Api.HealthCheck.Service.L0.Test.Checks
{
    public class PostgreSqlServerHealthCheckTests
    {
        [Fact]
        public async Task CheckHealthAsync_WithValidDbConnectionMockReturn_Healthy()
        {
            // Arrange
            var healthCheckContextMock = new HealthCheckContext();

            var dbConnectionMock = new Mock<IDbConnection>();

            var dbCommandMock = new Mock<IDbCommand>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(1);

            dbConnectionMock.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);

            var postgreSqlServerHealthCheck = new PostgreSqlServerHealthCheck(dbConnectionMock.Object);

            // Act
            var result = await postgreSqlServerHealthCheck.CheckHealthAsync(healthCheckContextMock);

            // Assert
            Assert.Equal(HealthStatus.Healthy, result.Status);
        }

        [Fact]
        public async Task CheckHealthAsync_WithInValidDbConnectionMockReturn_UnHealthy()
        {
            // Arrange
            var healthCheckContextMock = new HealthCheckContext();

            var dbConnectionMock = new Mock<IDbConnection>();

            var dbCommandMock = new Mock<IDbCommand>();

            dbCommandMock.Setup(c => c.ExecuteScalar()).Returns(0);

            dbConnectionMock.Setup(c => c.CreateCommand()).Returns(dbCommandMock.Object);

            var postgreSqlServerHealthCheck = new PostgreSqlServerHealthCheck(dbConnectionMock.Object);

            // Act
            var result = await postgreSqlServerHealthCheck.CheckHealthAsync(healthCheckContextMock);

            // Assert
            Assert.Equal(HealthStatus.Unhealthy, result.Status);
        }

        [Fact]
        public async Task CheckHealthAsync_WithExceptionThrown_UnHealthy()
        {
            // Arrange
            var healthCheckContextMock = new HealthCheckContext();

            var dbConnectionMock = new Mock<IDbConnection>();

            dbConnectionMock.Setup(c => c.CreateCommand()).Throws(new Exception("Mock exception"));

            var postgreSqlServerHealthCheck = new PostgreSqlServerHealthCheck(dbConnectionMock.Object);

            // Act
            var result = await postgreSqlServerHealthCheck.CheckHealthAsync(healthCheckContextMock);

            // Assert
            Assert.Equal(HealthStatus.Unhealthy, result.Status);
        }
    }
}
