﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Api.HealthCheck.Service.L0.Test
{
    public class PostgreSqlServerHealthCheckRegistrationTests
    {
        [Fact]
        public void AddPostgreSqlServerCheck_WithValidParameters_Succeeds()
        {
            // Arrange
            var serviceCollectionMock = new ServiceCollection();

            var healthCheckBuilderMock = new Mock<IHealthChecksBuilder>();

            healthCheckBuilderMock.Setup(c => c.Services).Returns(serviceCollectionMock);

            const string connectionStringMock = "Server=mockpsql; Port=5432; Database=mockDb; User Id=mockUserId; Password=mockPassword; Timeout=300";

            // Act
            var result = healthCheckBuilderMock.Object.AddPostgreSqlServerCheck("mock name", connectionStringMock);

            // Asset
            Assert.IsAssignableFrom<IHealthChecksBuilder>(result);
            Assert.True(serviceCollectionMock.Count > 0);
        }
    }
}
