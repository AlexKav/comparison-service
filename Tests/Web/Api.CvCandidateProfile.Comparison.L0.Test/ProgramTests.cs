﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace Api.CvCandidateProfile.Comparison.L0.Test
{
    public class ProgramTests
    {
        [Fact]
        public void CurrentEnvironment()
        {
            // Arrange

            // Act
            var result = Program.CurrentEnvironment;

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void CreateWebHostBuilder()
        {
            // Arrange
            var configurationMock = new Mock<IConfiguration>();

            // Act
            var result = Program.CreateWebHostBuilder(configurationMock.Object);

            // Assert
            Assert.IsAssignableFrom<IWebHostBuilder>(result);
        }
    }
}
