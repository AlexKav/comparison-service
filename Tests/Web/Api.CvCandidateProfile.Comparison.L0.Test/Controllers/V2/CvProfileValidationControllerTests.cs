﻿using System;
using System.Collections.Generic;

using Api.CvCandidateProfile.Comparison.Controllers.V2;
using Api.CvCandidateProfile.Comparison.Models.Request;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Api.CvCandidateProfile.Comparison.L0.Test.Controllers.V2
{
    public class CvProfileValidationControllerTests
    {
        [Fact]
        public void CvProfileValidationController_comparisonServiceIsNull_ArgumentNullExceptionThrown()
        {
            // Arrange
            const IComparisonService comparisonService = null;

            // Act
            CvProfileValidationController Action() => new CvProfileValidationController(comparisonService);

            // Assert
            Assert.Throws<ArgumentNullException>(
                nameof(comparisonService),
                (Func<CvProfileValidationController>)Action);
        }

        [Fact]
        public void UpdateProfileReviewStagesTest_WithValidInput_Succeeds()
        {
            // Arrange
            var comparisonServiceMock = new Mock<IComparisonService>();
            var request = new ProfileReviewStagesRequest();

            comparisonServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<IList<Stage>>())).Returns(true);

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            var result = cvProfileValidationController.UpdateProfileReviewStages(request);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(typeof(OkObjectResult), result.GetType());
        }

        [Fact]
        public void UpdateProfileReviewStagesTest_InvalidRequestExceptionThrown_ArgumentNullException()
        {
            // Arrange
            var comparisonServiceMock = new Mock<IComparisonService>();

            comparisonServiceMock.Setup(x => x.UpdateProfileReviewStages(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<IList<Stage>>())).Returns(true);

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => cvProfileValidationController.UpdateProfileReviewStages(null));
        }

        [Fact]
        public void IsCandidateProfileReviewFinished_WithValidInput_Succeeds()
        {
            // Arrange
            const string candidateId = "34";

            var comparisonServiceMock = new Mock<IComparisonService>();

            comparisonServiceMock.Setup(x => x.IsCandidateProfileReviewFinished(It.IsAny<int>())).Returns(true);

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            var result = cvProfileValidationController.IsCandidateProfileReviewFinished(candidateId) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.IsType<bool>(result.Value);
        }
    }
}
