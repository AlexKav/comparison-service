﻿using System.Collections.Generic;

using Api.CvCandidateProfile.Comparison.Controllers.V1;
using Api.CvCandidateProfile.Comparison.Models.Response;
using Api.Hangfire.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Api.CvCandidateProfile.Comparison.L0.Test.Controllers.V1
{
    public class HangfireControllerTests
    {
        [Fact]
        public void Servers()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var hangfireServersMock = new List<string> { "mock 1", "mock 2" };

            hangfireServiceMock.Setup(c => c.Servers()).Returns(hangfireServersMock);

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.Servers().Result as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.Servers());
            Assert.NotNull(result?.Value);
            Assert.IsType<MultiResultResponseModel<string>>(result.Value);
            Assert.Equal(hangfireServersMock.Count, ((MultiResultResponseModel<string>)result.Value).Result.Count);
        }

        [Fact]
        public void DeleteServers()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var hangfireServersMock = new List<string> { "mock 1", "mock 2" };

            hangfireServiceMock.Setup(c => c.Servers()).Returns(hangfireServersMock);

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.DeleteServers(hangfireServersMock.ToArray()) as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.DeleteServers(It.IsAny<string[]>()));
            Assert.NotNull(result);
            Assert.IsType<VoidResultResponseModel>(result.Value);
        }

        [Fact]
        public void Jobs()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var hangfireServersMock = new List<string> { "mock 1", "mock 2" };

            hangfireServiceMock.Setup(c => c.Jobs()).Returns(hangfireServersMock);

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.Jobs().Result as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.Jobs());
            Assert.NotNull(result?.Value);
            Assert.IsType<MultiResultResponseModel<string>>(result.Value);
            Assert.Equal(hangfireServersMock.Count, ((MultiResultResponseModel<string>)result.Value).Result.Count);
        }

        [Fact]
        public void DeleteJobs()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var hangfireServersMock = new List<string> { "mock 1", "mock 2" };

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.DeleteJobs(hangfireServersMock.ToArray()) as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.DeleteJobs(It.IsAny<string[]>()));
            Assert.NotNull(result);
            Assert.IsType<VoidResultResponseModel>(result.Value);
        }

        [Fact]
        public void RecurringJobs()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var hangfireServersMock = new List<string> { "mock 1", "mock 2" };

            hangfireServiceMock.Setup(c => c.RecurringJobs()).Returns(hangfireServersMock);

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.RecurringJobs().Result as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.RecurringJobs());
            Assert.NotNull(result?.Value);
            Assert.IsType<MultiResultResponseModel<string>>(result.Value);
            Assert.Equal(hangfireServersMock.Count, ((MultiResultResponseModel<string>)result.Value).Result.Count);
        }

        [Fact]
        public void DeleteRecurringJobs()
        {
            // Arrange
            var hangfireServiceMock = new Mock<IHangfireService>();

            var jobsMock = new List<string> { "mock 1", "mock 2" };

            var hangfireController = new HangfireController(hangfireServiceMock.Object);

            // Act
            var result = hangfireController.DeleteRecurringJobs(jobsMock.ToArray()) as OkObjectResult;

            // Assert
            hangfireServiceMock.Verify(c => c.DeleteRecurringJobs(It.IsAny<string[]>()));
            Assert.NotNull(result);
            Assert.IsType<VoidResultResponseModel>(result.Value);
        }
    }
}
