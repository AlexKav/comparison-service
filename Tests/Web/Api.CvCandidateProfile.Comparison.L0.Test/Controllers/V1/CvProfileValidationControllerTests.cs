﻿using System;
using System.Threading.Tasks;

using Amazon.SecretsManager.Model;
using Api.CvCandidateProfile.Comparison.Controllers.V1;

using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1;

using Microsoft.AspNetCore.Mvc;
using Moq;

using Xunit;

namespace Api.CvCandidateProfile.Comparison.L0.Test.Controllers.V1
{
    public class CvProfileValidationControllerTests
    {
        [Fact]
        public void CvProfileValidationController_comparisonServiceIsNull_ArgumentNullExceptionThrown()
        {
            // Arrange
            const IComparisonService comparisonService = null;

            // Act
            CvProfileValidationController Action() => new CvProfileValidationController(comparisonService);

            // Assert
            Assert.Throws<ArgumentNullException>(
                nameof(comparisonService),
                (Func<CvProfileValidationController>)Action);
        }

        [Fact]
        public async Task CvProfileResponseModelAsyncTest_WithValidInput_Succeeds()
        {
            // Arrange
            const string candidateId = "34";

            var comparisonServiceMock = new Mock<IComparisonService>();
            var cvProfileResponseModel = new CvProfileResponseModel();

            comparisonServiceMock.Setup(x => x.CompareProfilesAsync(It.IsAny<int>())).ReturnsAsync(cvProfileResponseModel);

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            var result = await cvProfileValidationController.CvProfileResponseModelAsync(candidateId) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.IsType<CvProfileResponseModel>(result.Value);
        }

        [Fact]
        public async Task CvProfileResponseModelAsyncTest_WithInValidInput_ThrowsException()
        {
            // Arrange
            const string wrongCandidateId = "mock 1";

            var comparisonServiceMock = new Mock<IComparisonService>();
            var cvProfileResponseModel = new CvProfileResponseModel();
            comparisonServiceMock.Setup(x => x.CompareProfilesAsync(It.IsAny<int>())).ReturnsAsync(cvProfileResponseModel);

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            // Assert
            await Assert.ThrowsAsync<FormatException>(async () => await cvProfileValidationController.CvProfileResponseModelAsync(wrongCandidateId));
        }

        [Fact]
        public async Task CvProfileResponseModelAsyncTest_InvalidRequestExceptionThrown_BadRequestStatusReturned()
        {
            // Arrange
            const string candidateId = "35";
            const string invalidRequestExceptionMessage = "mock InvalidRequestException Message";

            var comparisonServiceMock = new Mock<IComparisonService>();

            comparisonServiceMock
                .Setup(x => x.CompareProfilesAsync(It.IsAny<int>()))
                .ThrowsAsync(new InvalidRequestException(invalidRequestExceptionMessage));

            var cvProfileValidationController = new CvProfileValidationController(comparisonServiceMock.Object);

            // Act
            // Assert
            await Assert.ThrowsAsync<InvalidRequestException>(async () => await cvProfileValidationController.CvProfileResponseModelAsync(candidateId));
        }
    }
}
