﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Api.CorrelationData.Service;
using Api.CvCandidateProfile.Comparison.Middleware;
using Api.CvCandidateProfile.Comparison.Models.Response;
using Api.CvCandidateProfile.Comparison.Settings;
using Api.TaskManager.Service.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace Api.CvCandidateProfile.Comparison.L0.Test.Middleware
{
    public class ExceptionMiddlewareTests
    {
        [Fact]
        public async Task NoExceptions()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Response.Body = new MemoryStream();

            var exceptionMiddleware = new ExceptionMiddleware(
                next =>
                {
                    next.Response.StatusCode = (int)HttpStatusCode.OK;

                    return next.Response.WriteAsync(HttpStatusCode.OK.ToString());
                },
                Mock.Of<ILogger<ExceptionMiddleware>>(),
                Mock.Of<ICorrelationDataService>(),
                Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(HttpStatusCode.OK.ToString(), new StreamReader(result.Body).ReadToEnd());
        }

        [Fact]
        public async Task Exception()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new Exception("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.InternalServerError, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task ArgumentException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new ArgumentException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task InvalidOperationException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new InvalidOperationException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task KeyNotFoundException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new KeyNotFoundException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task IndexOutOfRangeException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new IndexOutOfRangeException("A mock exception");

            var exceptionMiddlewareMock = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddlewareMock.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task NullReferenceException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegated(HttpContext context) => throw new NullReferenceException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegated, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.NotFound, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task AccessViolationException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new AccessViolationException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.FailedDependency, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }

        [Fact]
        public async Task ExternalException()
        {
            // Arrange
            var httpContextMock = new DefaultHttpContext();

            httpContextMock.Request.Headers.Add("X-Forwarded-For", "192.168.1.1");

            httpContextMock.Response.Body = new MemoryStream();

            Task RequestDelegatedMock(HttpContext context) => throw new ExternalException("A mock exception");

            var exceptionMiddleware = new ExceptionMiddleware(RequestDelegatedMock, Mock.Of<ILogger<ExceptionMiddleware>>(), Mock.Of<ICorrelationDataService>(), Mock.Of<IApplicationSettings>());

            // Act
            await exceptionMiddleware.InvokeAsync(httpContextMock);

            httpContextMock.Response.Body.Seek(0, SeekOrigin.Begin);

            var result = httpContextMock.Response;

            // Assert
            Assert.Equal((int)HttpStatusCode.FailedDependency, result.StatusCode);
            Assert.NotNull(JsonConvert.DeserializeObject<ExceptionResponseModel>(new StreamReader(result.Body).ReadToEnd()));
        }
    }
}
