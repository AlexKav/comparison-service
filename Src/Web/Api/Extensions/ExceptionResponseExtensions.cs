﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;

using Api.CvCandidateProfile.Comparison.Models.Response;

namespace Api.CvCandidateProfile.Comparison.Extensions
{
    public static class ExceptionResponseExtensions
    {
        public static ExceptionResponseModel ToExceptionResponseModel(this Exception exception)
        {
            return new ExceptionResponseModel(exception?.Message, exception?.Source, exception.ToHttpStatusCode());
        }

        public static HttpStatusCode ToHttpStatusCode(this Exception exception)
        {
            var maps = new Dictionary<Type, HttpStatusCode>
            {
                { typeof(ArgumentException), HttpStatusCode.BadRequest },
                { typeof(ArgumentNullException), HttpStatusCode.BadRequest },
                { typeof(InvalidOperationException), HttpStatusCode.BadRequest },
                { typeof(FormatException), HttpStatusCode.BadRequest },
                { typeof(KeyNotFoundException), HttpStatusCode.NotFound },
                { typeof(IndexOutOfRangeException), HttpStatusCode.NotFound },
                { typeof(NullReferenceException), HttpStatusCode.NotFound },
                { typeof(AccessViolationException), HttpStatusCode.FailedDependency },
                { typeof(ExternalException), HttpStatusCode.FailedDependency }
            };

            if (!maps.TryGetValue(exception.GetType(), out var httpStatusCode))
            {
                httpStatusCode = HttpStatusCode.InternalServerError;
            }

            return httpStatusCode;
        }
    }
}
