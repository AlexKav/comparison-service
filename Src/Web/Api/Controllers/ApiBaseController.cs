﻿using System.Net;

using Api.CvCandidateProfile.Comparison.Attributes;
using Api.CvCandidateProfile.Comparison.Models.Response;
using Microsoft.AspNetCore.Mvc;

namespace Api.CvCandidateProfile.Comparison.Controllers
{
    [ApiAuthorize]
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(ValidationProblemDetails), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(UnauthorizedResponseModel), (int)HttpStatusCode.Unauthorized)]
    [ProducesResponseType(typeof(ExceptionResponseModel), (int)HttpStatusCode.InternalServerError)]
    public class ApiBaseController : ControllerBase
    {
    }
}
