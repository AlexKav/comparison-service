﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Api.CvCandidateProfile.Comparison.Attributes;
using Api.CvCandidateProfile.Comparison.Models.Request;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;

using Microsoft.AspNetCore.Mvc;

namespace Api.CvCandidateProfile.Comparison.Controllers.V2
{
    [Route("")]
    [ApiVersion("2.0")]
    [EventTimer]
    public class CvProfileValidationController : ApiBaseController
    {
        private readonly IComparisonService _comparisonService;

        public CvProfileValidationController(IComparisonService comparisonService)
        {
            _comparisonService = comparisonService ?? throw new ArgumentNullException(nameof(comparisonService));
        }

        /// <summary>
        /// Get cv profile compared model
        /// </summary>
        /// <param name="request">Incoming request with candidate Id and candidate profile json string</param>
        /// <returns>cv profile compared model</returns>
        [HttpPost]
        [Route("v{version:apiVersion}/return-cv-profile-compared-result")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(typeof(CvProfileResponseModelV2), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CandidateProfileResponseModel(
            [FromBody, Required] CandidateProfileRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var id = int.Parse(request.CandidateId);
            var cvProfileResponseModel = await _comparisonService.CompareProfilesV2Async(id, request.ProfileJson);

            return Ok(cvProfileResponseModel);
        }

        [HttpPost]
        [Route("update-profile-review-stages")]
        [MapToApiVersion("2.0")]
        public IActionResult UpdateProfileReviewStages([FromBody, Required] ProfileReviewStagesRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var result = _comparisonService.UpdateProfileReviewStages(request.CandidateId, request.DaxtraStoreId, request.PropertyStageStatuses);
            return Ok(result);
        }

        /// <summary>
        /// Get cv profile compared model
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>cv profile compared model</returns>
        [HttpGet]
        [Route("is-candidate-profile-review-finished/{candidateId}")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public IActionResult IsCandidateProfileReviewFinished([FromRoute, Required] string candidateId)
        {
            var id = int.Parse(candidateId);
            var isReviewFinished = _comparisonService.IsCandidateProfileReviewFinished(id);

            return Ok(isReviewFinished);
        }
    }
}
