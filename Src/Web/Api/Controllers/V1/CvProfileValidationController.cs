﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

using Api.CvCandidateProfile.Comparison.Attributes;
using Api.CvCandidateProfile.Comparison.Models.Request;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1;

using Microsoft.AspNetCore.Mvc;

namespace Api.CvCandidateProfile.Comparison.Controllers.V1
{
    [Route("")]
    [ApiVersion("1.0")]
    [EventTimer]
    public class CvProfileValidationController : ApiBaseController
    {
        private readonly IComparisonService _comparisonService;

        public CvProfileValidationController(IComparisonService comparisonService)
        {
            _comparisonService = comparisonService ?? throw new ArgumentNullException(nameof(comparisonService));
        }

        /// <summary>
        /// Get cv profile compared model
        /// </summary>
        /// <param name="candidateId">Candidate Id</param>
        /// <returns>cv profile compared model</returns>
        [HttpGet]
        [Route("get-cv-profile-compared-result/{candidateId}")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(CvProfileResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CvProfileResponseModelAsync(
            [FromRoute, Required] string candidateId)
        {
            var id = int.Parse(candidateId);
            var cvProfileResponseModel = await _comparisonService.CompareProfilesAsync(id);

            return Ok(cvProfileResponseModel);
        }

        /// <summary>
        /// Get cv profile compared model
        /// </summary>
        /// <param name="request">Incoming request with candidate Id and candidate profile json string</param>
        /// <returns>cv profile compared model</returns>
        [HttpPost]
        [Route("return-cv-profile-compared-result")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(CvProfileResponseModel), (int)HttpStatusCode.OK)]
        public IActionResult CandidateProfileResponseModel(
            [FromBody, Required] CandidateProfileRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var id = int.Parse(request.CandidateId);
            var cvProfileResponseModel = _comparisonService.CompareProfiles(id, request.ProfileJson);

            return Ok(cvProfileResponseModel);
        }
    }
}
