﻿using System.ComponentModel.DataAnnotations;
using System.Net;

using Api.CvCandidateProfile.Comparison.Attributes;
using Api.CvCandidateProfile.Comparison.Models.Response;
using Api.Hangfire.Service.Interfaces;

using Microsoft.AspNetCore.Mvc;

namespace Api.CvCandidateProfile.Comparison.Controllers.V1
{
    [Route("hangfire")]
    [ApiVersion("1.0")]
    [EventCounter]
    [EventTimer]
    public class HangfireController : ApiBaseController
    {
        private readonly IHangfireService _hangfireService;

        public HangfireController(IHangfireService hangfireService)
        {
            _hangfireService = hangfireService;
        }

        /// <summary>
        /// Gets all task server ids
        /// </summary>
        /// <returns>A list of task server ids</returns>
        [HttpGet]
        [Route("servers")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(MultiResultResponseModel<string>), (int)HttpStatusCode.OK)]
        public ActionResult<MultiResultResponseModel<string>> Servers()
        {
            return Ok(new MultiResultResponseModel<string>(_hangfireService.Servers()));
        }

        /// <summary>
        /// Purges the servers.
        /// </summary>
        /// <param name="serverIds">The server ids.</param>
        [HttpDelete]
        [Route("servers/delete")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(VoidResultResponseModel), (int)HttpStatusCode.OK)]
        public ActionResult DeleteServers([Required, MinLength(1)] string[] serverIds)
        {
            _hangfireService.DeleteServers(serverIds);

            return Ok(new VoidResultResponseModel());
        }

        /// <summary>
        /// Gets all background job ids.
        /// </summary>
        /// <returns>A list of background job ids.</returns>
        [HttpGet]
        [Route("jobs")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(MultiResultResponseModel<string>), (int)HttpStatusCode.OK)]
        public ActionResult<MultiResultResponseModel<string>> Jobs()
        {
            return Ok(new MultiResultResponseModel<string>(_hangfireService.Jobs()));
        }

        /// <summary>
        /// Purges the jobs.
        /// </summary>
        /// <param name="jobIds">The job ids.</param>
        [HttpDelete]
        [Route("jobs/delete")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(VoidResultResponseModel), (int)HttpStatusCode.OK)]
        public ActionResult DeleteJobs([Required, MinLength(1)] string[] jobIds)
        {
            _hangfireService.DeleteJobs(jobIds);

            return Ok(new VoidResultResponseModel());
        }

        /// <summary>
        /// Gets all recurring jobs ids.
        /// </summary>
        /// <returns>Ok Object result</returns>
        [HttpGet]
        [Route("jobs/recurring")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(MultiResultResponseModel<string>), (int)HttpStatusCode.OK)]
        public ActionResult<MultiResultResponseModel<string>> RecurringJobs()
        {
            return Ok(new MultiResultResponseModel<string>(_hangfireService.RecurringJobs()));
        }

        /// <summary>
        /// Purges the recurring jobs.
        /// </summary>
        /// <param name="jobIds">The job ids.</param>
        /// <returns>Ok Object result</returns>
        [HttpDelete]
        [Route("jobs/recurring/delete")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(VoidResultResponseModel), (int)HttpStatusCode.OK)]
        public ActionResult DeleteRecurringJobs([Required, MinLength(1)] string[] jobIds)
        {
            _hangfireService.DeleteRecurringJobs(jobIds);

            return Ok(new VoidResultResponseModel());
        }
    }
}
