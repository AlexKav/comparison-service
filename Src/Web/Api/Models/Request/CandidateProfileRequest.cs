﻿namespace Api.CvCandidateProfile.Comparison.Models.Request
{
    public class CandidateProfileRequest
    {
        public string CandidateId { get; set; }

        public string ProfileJson { get; set; }
    }
}
