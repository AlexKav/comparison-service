﻿using System.Collections.Generic;

using CvCandidateProfile.Comparison.Service.Models;

namespace Api.CvCandidateProfile.Comparison.Models.Request
{
    public class ProfileReviewStagesRequest
    {
        public int CandidateId { get; set; }

        public int DaxtraStoreId { get; set; }

        public IEnumerable<Stage> PropertyStageStatuses { get; set; }
    }
}
