﻿using System;
using System.IO;

using Api.CvCandidateProfile.Comparison.Extensions.Startup;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Api.CvCandidateProfile.Comparison
{
    public static class Program
    {
        public static string CurrentEnvironment => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

        public static void Main(string[] args)
        {
            BuildWebHost(args, CurrentEnvironment).Run();
        }

        public static IWebHost BuildWebHost(string[] args, string environment)
        {
            var configuration = CreateConfigurationBuilder(args, environment)
                .Build();

            var webHost = CreateWebHostBuilder(configuration)
                .Build();

            return webHost;
        }

        public static IConfigurationBuilder CreateConfigurationBuilder(string[] args, string environment)
        {
            return new ConfigurationBuilder()
                .AddApplicationPath(environment)
                .AddApplicationSettings(environment)
                .AddSecrets(environment)
                .AddCommandLine(args);
        }

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration configuration)
        {
            return new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel()
                .UseIISIntegration()
                .UseConfiguration(configuration)
                .UseStartup<Startup>();
        }
    }
}
