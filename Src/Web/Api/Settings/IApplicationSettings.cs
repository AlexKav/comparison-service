﻿using Api.CvCandidateProfile.Comparison.Settings.Models;

namespace Api.CvCandidateProfile.Comparison.Settings
{
    public interface IApplicationSettings
    {
        Application Application { get; }

        Secrets Secrets { get; }

        Framework Framework { get; }

        Oauth Oauth { get; }

        Documentation Documentation { get; }

        Models.Logs Logs { get; }

        Cors Cors { get; }

        HealthChecks HealthChecks { get; }

        Cache Cache { get; }

        Models.Hangfire Hangfire { get; }

        PostgreSqlConnection PostgreSqlConnection { get; }

        CandidateProfileSettings CandidateProfileSettings { get; }
    }
}
