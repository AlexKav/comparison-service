﻿using Api.CvCandidateProfile.Comparison.Settings.Models;

namespace Api.CvCandidateProfile.Comparison.Settings
{
    public class ApplicationSettings : IApplicationSettings
    {
        public Application Application { get; set; }

        public Secrets Secrets { get; set; }

        public Framework Framework { get; set; }

        public Oauth Oauth { get; set; }

        public Documentation Documentation { get; set; }

        public Models.Logs Logs { get; set; }

        public Cors Cors { get; set; }

        public HealthChecks HealthChecks { get; set; }

        public Cache Cache { get; set; }

        public Models.Hangfire Hangfire { get; set; }

        public PostgreSqlConnection PostgreSqlConnection { get; set; }

        public CandidateProfileSettings CandidateProfileSettings { get; set; }
    }
}
