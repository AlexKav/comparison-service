﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class PostgreSqlConnection
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string ConnectionString { get; set; }

        public string ProviderName { get; set; }
    }
}
