﻿using System;
using System.Collections.Generic;

namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Oauth
    {
        public string Provider { get; set; }

        public string Audience { get; set; }

        public OauthFlow[] Flows { get; set; }

        public OauthEndPoints EndPoints { get; set; }

        public OauthClient[] Clients { get; set; }

        public OauthProxy Proxy { get; set; }

        public class OauthProxy
        {
            public string Host { get; set; }
        }

        public class OauthClient
        {
            public string ClientId { get; set; }

            public string ClientName { get; set; }

            public string Secret { get; set; }

            #pragma warning disable S4004
            public IDictionary<string, string> Scopes { get; set; }
            #pragma warning restore S4004

            public OauthFlow Flow { get; set; }
        }

        public class OauthEndPoints
        {
            public Uri IssuerUrl { get; set; }

            public string Authority { get; set; }

            public Uri TokenUrl { get; set; }

            public Uri AuthorizationUrl { get; set; }
        }

        public enum OauthFlow
        {
            ClientCredentials,
            Password,
            Implicit
        }
    }
}
