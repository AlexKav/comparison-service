﻿using System;

using Microsoft.Extensions.Logging;

namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Logs
    {
        public FileLogProvider[] FileProviders { get; set; }

        public ElasticLogProvider[] ElasticProviders { get; set; }

        public DataDogProvider[] DataDogProviders { get; set; }

        public SlackProvider[] SlackProviders { get; set; }

        public class ElasticLogProvider
        {
            public string IndexPattern { get; set; }

            public Uri NodeUrl { get; set; }

            public string Username { get; set; }

            public string Password { get; set; }

            public int NoOfShards { get; set; }

            public int NoOfReplicas { get; set; }

            public LogLevel LogLevel { get; set; }

            public string[] LogTypes { get; set; }
        }

        public class FileLogProvider
        {
            public string FileDirectory { get; set; }

            public string FilePattern { get; set; }

            public LogLevel LogLevel { get; set; }

            public string[] LogTypes { get; set; }
        }

        public class DataDogProvider
        {
            public string Server { get; set; }

            public string Prefix { get; set; }

            public LogLevel LogLevel { get; set; }

            public string[] LogTypes { get; set; }
        }

        public class SlackProvider
        {
            public string WebhookUrl { get; set; }
        }
    }
}
