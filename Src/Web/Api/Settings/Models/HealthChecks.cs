﻿using System;

namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class HealthChecks
    {
        public Check Checks { get; set; }

        public Publisher Publishers { get; set; }

        public class Check
        {
            public RedisCheck[] Redis { get; set; }

            public ElasticSearchCheck[] ElasticSearch { get; set; }

            public OAuthCheck[] OAuth { get; set; }

            public SqlServerCheck[] SqlServer { get; set; }

            public SqsCheck[] Sqs { get; set; }

            public DynamoDbCheck[] DynamoDb { get; set; }

            public PostgreSqlCheck PostgreSql { get; set; }

            public class RedisCheck
            {
                public string Name { get; set; }

                public string ConnectionString { get; set; }

                public bool IsActive { get; set; }
            }

            public class ElasticSearchCheck
            {
                public string Name { get; set; }

                public Uri ConnectionString { get; set; }

                public string UserName { get; set; }

                public string Password { get; set; }

                public bool IsActive { get; set; }
            }

            public class OAuthCheck
            {
                public string Name { get; set; }

                public Uri ConnectionString { get; set; }

                public bool IsActive { get; set; }
            }

            public class SqlServerCheck
            {
                public string Name { get; set; }

                public string ConnectionString { get; set; }

                public bool IsActive { get; set; }
            }

            public class SqsCheck
            {
                public string Name { get; set; }

                public string QueueName { get; set; }

                public string AwsKeyId { get; set; }

                public string AwsSecretKey { get; set; }

                public string Region { get; set; }

                public bool IsActive { get; set; }
            }

            public class DynamoDbCheck
            {
                public string Name { get; set; }

                public string AwsKeyId { get; set; }

                public string AwsSecretKey { get; set; }

                public string Region { get; set; }

                public bool IsActive { get; set; }
            }

            public class PostgreSqlCheck
            {
                public string Name { get; set; }

                public string ConnectionString { get; set; }

                public bool IsActive { get; set; }
            }
        }

        public class Publisher
        {
            public string Slack { get; set; }
        }
    }
}
