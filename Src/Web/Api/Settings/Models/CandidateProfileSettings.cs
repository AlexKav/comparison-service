﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class CandidateProfileSettings
    {
        public string Cookie { get; set; }

        public string Url { get; set; }
    }
}
