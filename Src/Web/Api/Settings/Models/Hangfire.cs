﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Hangfire
    {
        public string StorageProvider { get; set; }

        public string StorageProviderConnection { get; set; }

        public int InvisibilityTimeout { get; set; }

        public int MaxDeletedListLength { get; set; }

        public int MaxSucceededListLength { get; set; }

        public string DataStoreName { get; set; }

        public string Dashboard { get; set; }

        public int WorkerCount { get; set; }

        public string[] Queues { get; set; }
    }
}
