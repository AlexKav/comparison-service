﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Cors
    {
        public bool IsActive { get; set; }

        public CorsPolicy Policy { get; set; }

        public class CorsPolicy
        {
            public string Name { get; set; }

            public string[] Origins { get; set; }
        }
    }
}
