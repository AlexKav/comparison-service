﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Documentation
    {
        public string OauthClientId { get; set; }

        public string Callback { get; set; }

        public string Dashboard { get; set; }

        public string DefinitionDocument { get; set; }

        public string Styles { get; set; }

        public string Path { get; set; }

        public string File { get; set; }

        public string Template { get; set; }
    }
}
