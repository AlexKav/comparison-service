﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Secrets
    {
        public AwsSecret Aws { get; set; }

        public class AwsSecret
        {
            public string Region { get; set; }

            public string Version { get; set; }

            public string[] Secrets { get; set; }
        }
    }
}
