﻿using Microsoft.AspNetCore.Mvc;

namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Framework
    {
        public CompatibilityVersion CompatibilityVersion { get; set; }
    }
}
