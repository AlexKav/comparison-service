﻿namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Cache
    {
        public string ConnectionString { get; set; }

        public bool IsActive { get; set; }
    }
}
