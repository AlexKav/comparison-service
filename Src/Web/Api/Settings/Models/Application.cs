﻿using System;

namespace Api.CvCandidateProfile.Comparison.Settings.Models
{
    public class Application
    {
        public string Name { get; set; }

        public Uri HostingUrl { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public ApplicationVersion Version { get; set; }

        public ApplicationVersion[] SupportedVersions { get; set; }

        public ApplicationHeaderData HttpHeaderData { get; set; }

        public ApplicationHeaderDataResponse HttpHeaderDataResponse { get; set; }

        public ApplicationIpAddressWhiteList IpAddressWhiteList { get; set; }

        public class ApplicationVersion
        {
            public int Major { get; set; }

            public int Minor { get; set; }
        }

        public class ApplicationHeaderData
        {
            public ApplicationHeaderCorrelationData CorrelationData { get; set; }

            public ApplicationHeaderVersionData VersionData { get; set; }

            public ApplicationHeaderAuthenticationData AuthenticationData { get; set; }

            public class ApplicationHeaderAuthenticationData
            {
                public ApplicationHeaderDataValue AuthorizationToken { get; set; }
            }

            public class ApplicationHeaderVersionData
            {
                public ApplicationHeaderDataValue Version { get; set; }
            }

            public class ApplicationHeaderCorrelationData
            {
                public ApplicationHeaderDataValue CorrelationId { get; set; }

                public ApplicationHeaderDataValue CorrelationSourceId { get; set; }

                public ApplicationHeaderDataValue CorrelationUserId { get; set; }

                public ApplicationHeaderDataValue CorrelationImpersonatedUserId { get; set; }
            }

            public class ApplicationHeaderDataValue
            {
                public string Key { get; set; }

                public string Description { get; set; }

                public bool IsRequired { get; set; }

                public string Type { get; set; }
            }
        }

        public class ApplicationHeaderDataResponse
        {
            public ApplicationHeaderDataValue ElapsedMilliseconds { get; set; }

            public class ApplicationHeaderDataValue
            {
                public string Key { get; set; }
            }
        }

        public class ApplicationIpAddressWhiteList
        {
            public bool IsActive { get; set; }

            public ApplicationIpAddressGroup[] AuthorizedAddresses { get; set; }
        }

        public class ApplicationIpAddressGroup
        {
            public string Group { get; set; }

            public string[] Addresses { get; set; }
        }
    }
}
