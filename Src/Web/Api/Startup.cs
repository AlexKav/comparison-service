﻿using System;

using Api.CvCandidateProfile.Comparison.Extensions.Application;
using Api.CvCandidateProfile.Comparison.Extensions.Services;
using Api.CvCandidateProfile.Comparison.Settings;
using Api.Hangfire.Service;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api.CvCandidateProfile.Comparison
{
    public class Startup : IStartup
    {
        private readonly IApplicationSettings _applicationSettings;

        public Startup(IConfiguration configuration)
        {
            _applicationSettings = configuration.Get<ApplicationSettings>();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.ConfigureCore(_applicationSettings);

            services.ConfigureSecurity(_applicationSettings);

            services.ConfigureSwagger(_applicationSettings);

            services.ConfigureTaskManager(_applicationSettings);

            services.ConfigureLogging(_applicationSettings);

            services.ConfigureHealthChecks(_applicationSettings);

            services.ConfigureCors(_applicationSettings);

            services.ConfigureCache(_applicationSettings);

            services.ConfigureHangfireService(_applicationSettings);

            services.ConfigurePostgreSqlService(_applicationSettings);

            services.ConfigureCandidateProfileService(_applicationSettings);

            return services.BuildServiceProvider();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseSecurity();

            app.UseEnvironments();

            app.UseCore();

            app.UseHealthChecks();

            app.UseDocumentation();

            app.UseSwagger();

            app.UseCrossOriginRequests();

            app.UseHangfireService();
        }
    }
}
