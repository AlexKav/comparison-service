﻿using System;
using System.Threading.Tasks;

using Api.CorrelationData.Service;
using Api.CvCandidateProfile.Comparison.Extensions;
using Api.CvCandidateProfile.Comparison.Settings;
using Api.Logs.Service;
using Api.Logs.Service.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Api.CvCandidateProfile.Comparison.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly ILogger _logger;
        private readonly ICorrelationDataService _correlationDataService;

        private readonly string[] _exceptionTags;
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, ICorrelationDataService correlationDataService, IApplicationSettings applicationSettingsService)
        {
            _next = next;

            _logger = logger;

            _correlationDataService = correlationDataService;

            _exceptionTags = new[] { applicationSettingsService?.Application?.Name };
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception exception)
            {
                var exceptionModel = new LogExceptionModel
                {
                    Message = exception.Message,
                    RemoteHost = httpContext.GetIpAddress().ToString(),
                    EventName = httpContext?.Request?.Path,
                    Tags = _exceptionTags,
                    CorrelationId = _correlationDataService.CorrelationId,
                    CorrelationSourceId = _correlationDataService.CorrelationSourceId,
                    CorrelationUserId = _correlationDataService.CorrelationUserId,
                    CorrelationImpersonatedUserId = _correlationDataService.CorrelationImpersonatedUserId,
                    Exception = exception.GetType().IsSerializable ? exception : new Exception(exception.Message)
                };

                _logger.LogException(exceptionModel);

                if (httpContext != null)
                {
                    var responseModel = exception.ToExceptionResponseModel();

                    httpContext.Response.ContentType = responseModel.ContentType;

                    httpContext.Response.StatusCode = (int)responseModel.StatusCode;

                    await httpContext.Response.WriteAsync(responseModel.ToString());

                    await httpContext.Response.Body.FlushAsync();
                }
            }
        }
    }
}
