﻿using Microsoft.Extensions.Options;

namespace CvCandidateProfile.PostgreSql.Service.Models
{
    public class PostgreSqlSettings : IOptions<PostgreSqlSettings>
    {
        public string ConnectionString { get; set; }

        public PostgreSqlSettings Value => this;
    }
}
