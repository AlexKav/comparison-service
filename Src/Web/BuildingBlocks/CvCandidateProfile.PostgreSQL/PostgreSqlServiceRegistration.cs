﻿using System;

using CvCandidateProfile.PostgreSql.Service.Interfaces;
using CvCandidateProfile.PostgreSql.Service.Models;

using Microsoft.Extensions.DependencyInjection;

namespace CvCandidateProfile.PostgreSql.Service
{
    public static class PostgreSqlServiceRegistration
    {
        public static void AddPostgreSqlService(
            this IServiceCollection serviceCollection,
            Action<PostgreSqlSettings> configurationAction,
            string connectionString)
        {
            serviceCollection.Configure(configurationAction);
            serviceCollection.AddSingleton(typeof(IPostgreSqlService), c => new PostgreSqlService(new DbConnectionFactory(connectionString)));
        }
    }
}
