﻿using System;
using System.Runtime.Serialization;

namespace CvCandidateProfile.PostgreSql.Service
{
    [Serializable]
    public sealed class PostgreSqlClientException : Exception
    {
        public PostgreSqlClientException()
        {
        }

        public PostgreSqlClientException(string message)
            : base(message)
        {
        }

        public PostgreSqlClientException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        private PostgreSqlClientException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
