﻿using System;
using System.Data;
using System.Threading.Tasks;

using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Npgsql;

using static System.String;

namespace CvCandidateProfile.PostgreSql.Service
{
    public class PostgreSqlService : IPostgreSqlService
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public PostgreSqlService(IDbConnectionFactory factory)
        {
            _connectionFactory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        public async Task<string> ReadDaxtraByIdAsync(int candidateId)
        {
            var candidateParseErrorMessage = $"Candidate {candidateId} parsed JSON data not found";
            using (var dbConnection = _connectionFactory.CreateConnection())
            {
                try
                {
                    var command = dbConnection.CreateCommand();

                    command.CommandType = CommandType.Text;

                    command.CommandText = Format("SELECT daxtraresult" +
                                                 " FROM daxtra.daxtrastore" +
                                                 $" WHERE candidateid = {candidateId}" +
                                                 " ORDER BY daxtrastoreid DESC" +
                                                 " LIMIT 1;");

                    dbConnection.Open();

                    var result = await Task.FromResult(command.ExecuteScalar()?.ToString());

                    if (IsNullOrEmpty(result))
                    {
                        throw new PostgreSqlClientException(candidateParseErrorMessage);
                    }

                    return result;
                }
                catch (Exception exception)
                {
                    throw new PostgreSqlClientException(candidateParseErrorMessage, exception);
                }
            }
        }

        public string ReadDaxtraById(int candidateId)
        {
            var candidateParseErrorMessage = $"Candidate {candidateId} parsed JSON data not found";
            using (var dbConnection = _connectionFactory.CreateConnection())
            {
                try
                {
                    var command = dbConnection.CreateCommand();

                    command.CommandType = CommandType.Text;

                    command.CommandText = Format("SELECT daxtraresult" +
                                                 " FROM daxtra.daxtrastore" +
                                                 $" WHERE candidateid = {candidateId}" +
                                                 " ORDER BY daxtrastoreid DESC" +
                                                 " LIMIT 1;");

                    dbConnection.Open();

                    var result = command.ExecuteScalar()?.ToString();

                    return result;
                }
                catch (Exception exception)
                {
                    throw new PostgreSqlClientException(candidateParseErrorMessage, exception);
                }
            }
        }

        public string ExecuteQuery(string query, int candidateId)
        {
            var candidateParseErrorMessage = $"Candidate {candidateId} parsed JSON data not found";
            using (var dbConnection = _connectionFactory.CreateConnection())
            {
                try
                {
                    var command = dbConnection.CreateCommand();

                    command.CommandType = CommandType.Text;

                    command.CommandText = query;

                    dbConnection.Open();

                    var result = command.ExecuteScalar()?.ToString();

                    return result;
                }
                catch (Exception exception)
                {
                    throw new PostgreSqlClientException(candidateParseErrorMessage, exception);
                }
            }
        }

        public string GetCandidateProfileValidationResults(int candidateId, int? daxtraStoreId = null)
        {
            var sqlQuery = daxtraStoreId == null
                ? "SELECT daxtraresultvalidation" +
                  " FROM daxtra.daxtrastore" +
                  $" WHERE candidateid = {candidateId}" +
                  " ORDER BY daxtrastoreid DESC" +
                  " LIMIT 1;"
                : "SELECT daxtraresultvalidation" +
                  " FROM daxtra.daxtrastore" +
                  $" WHERE candidateid = {candidateId} and daxtrastoreid={daxtraStoreId}" +
                  " LIMIT 1;";

            return ExecuteQuery(sqlQuery, candidateId);
        }

        public bool UpdateProfileReviewStages(int daxtraStoreId, string value)
        {
            using (var dbConnection = _connectionFactory.CreateConnection())
            {
                try
                {
                    var command = dbConnection.CreateCommand();

                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "daxtra.updatedaxtrastore";
                    command.Parameters.Add(new NpgsqlParameter("@daxtrastoreid", daxtraStoreId));
                    command.Parameters.Add(new NpgsqlParameter("@daxtraresultvalidation", value));
                    dbConnection.Open();
                    var result = command.ExecuteNonQuery();
                    return result == -1;
                }
                catch (Exception exception)
                {
                    throw new PostgreSqlClientException("daxtra.updatedaxtrastore", exception);
                }
            }
        }

        public int GetLanguageIdByLanguageCode(string value)
        {
            using (var dbConnection = _connectionFactory.CreateConnection())
            {
                try
                {
                    var command = dbConnection.CreateCommand();

                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "daxtra.languagecodeget";
                    command.Parameters.Add(new NpgsqlParameter("@languagecode", value));
                    dbConnection.Open();
                    var result = command.ExecuteScalar();
                    if (result == null)
                    {
                        return -1;
                    }
                    return int.Parse(result.ToString());
                }
                catch (Exception exception)
                {
                    throw new PostgreSqlClientException("daxtra.languagecodeget", exception);
                }
            }
        }
    }
}
