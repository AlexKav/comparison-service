﻿using System.Data;

namespace CvCandidateProfile.PostgreSql.Service.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
