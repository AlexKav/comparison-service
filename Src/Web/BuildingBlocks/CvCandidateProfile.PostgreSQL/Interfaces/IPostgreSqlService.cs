﻿using System.Threading.Tasks;

namespace CvCandidateProfile.PostgreSql.Service.Interfaces
{
    public interface IPostgreSqlService
    {
        Task<string> ReadDaxtraByIdAsync(int candidateId);

        string ReadDaxtraById(int candidateId);

        string ExecuteQuery(string query, int candidateId);

        bool UpdateProfileReviewStages(int daxtraStoreId, string value);

        string GetCandidateProfileValidationResults(int candidateId, int? daxtraStoreId = null);

        int GetLanguageIdByLanguageCode(string value);
    }
}
