﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Diagnostics.HealthChecks;

using Newtonsoft.Json;

namespace Api.HealthCheck.Service.Checks
{
    public class PostgreSqlServerHealthCheck : IHealthCheck
    {
        private readonly IDbConnection _dbConnection;

        public PostgreSqlServerHealthCheck(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context,
            CancellationToken cancellationToken = default)
        {
            try
            {
                _dbConnection.Open();

                var command = _dbConnection.CreateCommand();

                command.CommandType = CommandType.Text;

                command.CommandText = "SELECT 1";

                var result = await Task.FromResult((int)command.ExecuteScalar());

                return result != 1
                    ? HealthCheckResult.Unhealthy(HealthStatus.Unhealthy.ToString())
                    : HealthCheckResult.Healthy(HealthStatus.Healthy.ToString());
            }
            catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(JsonConvert.SerializeObject(ex));
            }
            finally
            {
                _dbConnection.Close();
            }
        }
    }
}
