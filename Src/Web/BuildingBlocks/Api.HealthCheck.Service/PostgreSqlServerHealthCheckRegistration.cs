﻿using Api.HealthCheck.Service.Checks;

using Microsoft.Extensions.DependencyInjection;

using Npgsql;

namespace Api.HealthCheck.Service
{
    public static class PostgreSqlServerHealthCheckRegistration
    {
        public static IHealthChecksBuilder AddPostgreSqlServerCheck(this IHealthChecksBuilder builder, string name, string connectionString)
        {
            var dbConnection = new NpgsqlConnection(connectionString);

            builder?.Services?.AddSingleton(typeof(PostgreSqlServerHealthCheck), c => new PostgreSqlServerHealthCheck(dbConnection));

            builder?.AddCheck<PostgreSqlServerHealthCheck>(name);

            return builder;
        }
    }
}
