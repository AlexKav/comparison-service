﻿using System;
using System.Collections.Generic;
using System.Linq;

using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;

namespace CvCandidateProfile.Comparison.Service
{
    public class LanguageFaсtory : ILanguageFactory
    {
        public LanguageField CreateLanguageFieldFromCv(Language languageItem, IEnumerable<LanguageField> resultProfileLanguages)
        {
            var profileLanguages = resultProfileLanguages as LanguageField[] ?? resultProfileLanguages.ToArray();
            return new LanguageField
            {
                LanguageId = new ProfileFieldV2
                {
                    JsonFieldName = "LanguageId",
                    JsonFieldContent = languageItem.LanguageId,
                    ProfileFieldName = "LanguageId",
                    ProfileFieldContents = profileLanguages
                        .FirstOrDefault(y => y.LanguageId.ProfileFieldContents == languageItem.LanguageId)
                        ?.LanguageId.ProfileFieldContents
                },
                Fluency = new ProfileFieldV2
                {
                    JsonFieldName = "Fluency",
                    JsonFieldContent = languageItem.Fluency.ToString(),
                    ProfileFieldName = "Fluency",
                    ProfileFieldContents = profileLanguages
                        .FirstOrDefault(y => y.LanguageId.ProfileFieldContents == languageItem.LanguageId)
                        ?.Fluency.ProfileFieldContents
                }
            };
        }

        public Func<Language, LanguageField> CreateLanguageFieldFromCvWithEmptyProfile()
        {
            return x => new LanguageField
            {
                LanguageId = new ProfileFieldV2
                {
                    JsonFieldName = "LanguageId",
                    JsonFieldContent = x.LanguageId,
                    ProfileFieldName = "LanguageId",
                    ProfileFieldContents = null
                },
                Fluency = new ProfileFieldV2
                {
                    JsonFieldName = "Fluency",
                    JsonFieldContent = x.Fluency.ToString(),
                    ProfileFieldName = "Fluency",
                    ProfileFieldContents = null
                }
            };
        }

        public LanguageField CreateLanguageFieldFromProfile(IEnumerable<Language> convertedLanguagesCv, Language languageItem)
        {
            var languagesCv = convertedLanguagesCv as Language[] ?? convertedLanguagesCv.ToArray();
            return new LanguageField
            {
                LanguageId = new ProfileFieldV2
                {
                    JsonFieldName = "LanguageId",
                    JsonFieldContent = languagesCv.FirstOrDefault(y => y.LanguageId == languageItem.LanguageId)
                        ?.LanguageId,
                    ProfileFieldName = "LanguageId",
                    ProfileFieldContents = languageItem.LanguageId
                },
                Fluency = new ProfileFieldV2
                {
                    JsonFieldName = "Fluency",
                    JsonFieldContent = languagesCv.FirstOrDefault(y => y.LanguageId == languageItem.LanguageId)
                        ?.Fluency
                        ?.ToString(),
                    ProfileFieldName = "Fluency",
                    ProfileFieldContents = languageItem.Fluency?.ToString()
                }
            };
        }
    }
}
