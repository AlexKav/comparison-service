﻿using System;
using System.Collections.Generic;

using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;

namespace CvCandidateProfile.Comparison.Service.Interfaces
{
    public interface ILanguageFactory
    {
        LanguageField CreateLanguageFieldFromCv(Language languageItem, IEnumerable<LanguageField> resultProfileLanguages);

        Func<Language, LanguageField> CreateLanguageFieldFromCvWithEmptyProfile();

        LanguageField CreateLanguageFieldFromProfile(IEnumerable<Language> convertedLanguagesCv, Language languageItem);
    }
}
