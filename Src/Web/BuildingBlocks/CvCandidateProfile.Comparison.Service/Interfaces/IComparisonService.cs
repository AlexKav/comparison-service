using System.Collections.Generic;
using System.Threading.Tasks;

using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;

namespace CvCandidateProfile.Comparison.Service.Interfaces
{
    public interface IComparisonService
    {
        Task<CvProfileResponseModel> CompareProfilesAsync(int candidateId);

        CvProfileResponseModel CompareProfiles(int candidateId, string candidateProfileJson);

        Task<CvProfileResponseModelV2> CompareProfilesV2Async(int candidateId, string candidateProfileJson);

        bool UpdateProfileReviewStages(int candidateId, int daxtraStoreId, IEnumerable<Stage> propertyStageStatuses);

        bool IsCandidateProfileReviewFinished(int candidateId);
    }
}
