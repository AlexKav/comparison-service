using System.Threading.Tasks;

namespace CvCandidateProfile.Comparison.Service.Interfaces
{
    public interface ICandidateProfileService
    {
        Task<string> GetJsonCandidateProfileAsync(int candidateId);
    }
}
