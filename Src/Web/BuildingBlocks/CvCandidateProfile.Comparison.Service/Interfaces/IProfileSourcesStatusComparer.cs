﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Interfaces
{
    public interface IProfileSourcesStatusComparer
    {
        ProfileSourcesStatus Compare<TValue1, TValue2>(TValue1 value1, TValue2 value2);
    }
}
