﻿using System;

namespace CvCandidateProfile.Comparison.Service.Extensions
{
    public static class ToEnumExtension
    {
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
