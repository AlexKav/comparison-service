﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Comparers
{
    public class ProfileSourcesStatusDateToComparer : Interfaces.IProfileSourcesStatusComparer
    {
        public ProfileSourcesStatus Compare<TValue1, TValue2>(TValue1 value1, TValue2 value2)
        {
            bool isProfile = value1 == null || !string.IsNullOrWhiteSpace(value1.ToString());

            bool isDaxtra = !string.IsNullOrWhiteSpace(value2?.ToString());

            if (isProfile && isDaxtra)
            {
                return ProfileSourcesStatus.ProfileAndDaxtraData;
            }

            if (isProfile)
            {
                return ProfileSourcesStatus.NoDaxtraData;
            }

            if (isDaxtra)
            {
                return ProfileSourcesStatus.NoProfileData;
            }

            return ProfileSourcesStatus.NoData;
        }
    }
}
