﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Comparers.Helpers
{
    public static class ProfileSourcesStatusHelper
    {
        public static ProfileSourcesStatus GetProfileSoursesStatus(string candidateProfileValue, string cvCandidateProfileValue)
        {
            bool isProfile = !string.IsNullOrWhiteSpace(candidateProfileValue);
            bool isDaxtra = !string.IsNullOrWhiteSpace(cvCandidateProfileValue);

            if (isProfile && isDaxtra)
            {
                return ProfileSourcesStatus.ProfileAndDaxtraData;
            }
            else if (isProfile)
            {
                return ProfileSourcesStatus.NoDaxtraData;
            }
            else if (isDaxtra)
            {
                return ProfileSourcesStatus.NoProfileData;
            }
            else
            {
                return ProfileSourcesStatus.NoData;
            }
        }

        public static ProfileSourcesStatus GetProfileSoursesStatus(string profileFieldContent, string jsonFieldContent, string jsonFieldName)
        {
            Interfaces.IProfileSourcesStatusComparer comparer;

            if (jsonFieldName == "EndDate")
            {
                comparer = new ProfileSourcesStatusDateToComparer();
            }
            else
            {
                comparer = new ProfileSourcesStatusStringComparer();
            }

            return comparer.Compare(profileFieldContent, jsonFieldContent);
        }
    }
}
