﻿using System;

using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Comparers.Helpers
{
    public static class ComparisonResultHelper
    {
        public static ComparisonResult Compare(string fieldValue, string cvFieldValue)
        {
            string value = fieldValue;
            string cvValue = cvFieldValue;
            if (string.IsNullOrWhiteSpace(fieldValue))
            {
                value = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(cvFieldValue))
            {
                cvValue = string.Empty;
            }

            return string.Equals(value, cvValue, StringComparison.InvariantCultureIgnoreCase) ? ComparisonResult.Match : ComparisonResult.NotExactMatch;
        }

        public static ComparisonResult Compare(string profileFieldContent, string jsonFieldContent, string jsonFieldName)
        {
            Interfaces.IComparer comparer;
            if (jsonFieldName == "EndDate")
            {
                comparer = new DateToComparer();
            }
            else
            {
                comparer = new CommonStringComparer();
            }

            return comparer.Compare(profileFieldContent, jsonFieldContent);
        }
    }
}
