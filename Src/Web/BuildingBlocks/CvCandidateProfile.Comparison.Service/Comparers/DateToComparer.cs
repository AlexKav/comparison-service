﻿using System;

using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Comparers
{
    public class DateToComparer : Interfaces.IComparer
    {
        public ComparisonResult Compare<TValue1, TValue2>(TValue1 value1, TValue2 value2)
        {
            string profileFieldContent = value1?.ToString();
            string jsonFieldContent = value2?.ToString();

            if (string.IsNullOrWhiteSpace(jsonFieldContent))
            {
                jsonFieldContent = string.Empty;
            }

            if (profileFieldContent == null &&
                jsonFieldContent.ToLowerInvariant() == "present")
            {
                return ComparisonResult.Match;
            }

            return string.Equals(profileFieldContent, jsonFieldContent, StringComparison.InvariantCultureIgnoreCase) ?
                ComparisonResult.Match :
                ComparisonResult.NotExactMatch;
        }
    }
}
