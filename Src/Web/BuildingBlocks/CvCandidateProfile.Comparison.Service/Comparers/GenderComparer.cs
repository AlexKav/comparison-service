﻿using System;

using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Comparers
{
    public class GenderComparer : Interfaces.IComparer
    {
        public ComparisonResult Compare<TValue1, TValue2>(TValue1 value1, TValue2 value2)
        {
            int intValue1 = Convert.ToInt32(value1);
            int intValue2 = Convert.ToInt32(value2);

            return intValue1 == intValue2 ? ComparisonResult.Match : ComparisonResult.NotExactMatch;
        }
    }
}
