﻿using System.Runtime.Serialization;

namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum ProfileGender
    {
        None = 0,
        [EnumMember(Value = "M")]
        Male,
        [EnumMember(Value = "F")]
        Female
    }
}
