﻿namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum ReviewStatus
    {
        NotChanged,
        CvAccepted,
        CvUpdated,
        ProfileAccepted,
        ProfileUpdated
    }
}
