﻿namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum LanguageProfileFluency
    {
       Fluent,
       Intermediate,
       Basic,
       None
    }
}
