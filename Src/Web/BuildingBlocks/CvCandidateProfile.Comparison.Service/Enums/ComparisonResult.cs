﻿using System.Runtime.Serialization;

namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum ComparisonResult
    {
        [EnumMember(Value = "match")]
        Match = 1,

        [EnumMember(Value = "not exact match")]
        NotExactMatch = 2
    }
}
