﻿namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum StageStatus
    {
        NotReviewed = 0,
        NoReviewNeeded = 1,
        Reviewed = 2,
    }
}
