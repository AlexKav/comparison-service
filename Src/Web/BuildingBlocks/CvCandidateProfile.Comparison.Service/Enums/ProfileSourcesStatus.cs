﻿using System.Runtime.Serialization;

namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum ProfileSourcesStatus
    {
        [EnumMember(Value = "profile and daxtra data")]
        ProfileAndDaxtraData = 1,

        [EnumMember(Value = "no profile data")]
        NoProfileData = 2,

        [EnumMember(Value = "no daxtra data")]
        NoDaxtraData = 3,

        [EnumMember(Value = "no profile and daxtra data")]
        NoData = 4
    }
}
