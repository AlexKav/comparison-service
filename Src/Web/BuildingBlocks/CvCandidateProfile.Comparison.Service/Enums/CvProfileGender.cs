﻿using System.Runtime.Serialization;

namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum CvProfileGender
    {
        None = 0,
        [EnumMember(Value = "Male")]
        Male,
        [EnumMember(Value = "Female")]
        Female
    }
}
