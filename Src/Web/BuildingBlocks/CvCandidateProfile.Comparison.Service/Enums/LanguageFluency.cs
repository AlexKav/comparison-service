﻿namespace CvCandidateProfile.Comparison.Service.Enums
{
    public enum LanguageFluency
    {
        None = 0,
        Native,
        Excellent,
        Fluent,
        Advanced,
        Intermediate,
        Basic
    }
}
