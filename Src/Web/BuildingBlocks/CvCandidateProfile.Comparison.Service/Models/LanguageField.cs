﻿using System;

using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;

namespace CvCandidateProfile.Comparison.Service.Models
{
    public class LanguageField : IEquatable<LanguageField>
    {
        public ProfileFieldV2 LanguageId { get; set; }

        public ProfileFieldV2 Fluency { get; set; }

        public bool Equals(LanguageField other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return Equals(LanguageId, other.LanguageId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((LanguageField)obj);
        }

        public override int GetHashCode()
        {
            return LanguageId != null ? LanguageId.GetHashCode() : 0;
        }
    }
}
