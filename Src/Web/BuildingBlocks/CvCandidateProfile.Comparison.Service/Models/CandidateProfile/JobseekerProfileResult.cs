﻿namespace CvCandidateProfile.Comparison.Service.Models.CandidateProfile
{
    public class JobseekerProfileResult
    {
        public Profile Profile { get; set; }

        public string Error { get; set; }
    }
}
