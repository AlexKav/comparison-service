﻿using System;

namespace CvCandidateProfile.Comparison.Service.Models.CandidateProfile
{
    public class WorkExperience
    {
        public string Company { get; set; }

        public string JobTitle { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Responsibilities { get; set; }
    }
}
