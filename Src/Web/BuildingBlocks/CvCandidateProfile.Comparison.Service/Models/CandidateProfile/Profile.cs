﻿using System.Collections.Generic;

using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;
using Newtonsoft.Json;

namespace CvCandidateProfile.Comparison.Service.Models.CandidateProfile
{
    public class Profile
    {
        public string Forename { get; set; }

        public string Surname { get; set; }

        public ProfileGender Gender { get; set; }

        [JsonProperty("Title")]
        public string JobTitle { get; set; }

        public string PersonalStatement { get; set; }

        public string IsGraduate { get; set; }

        public IEnumerable<WorkExperience> WorkExperience { get; set; }

        public IEnumerable<Language> Languages { get; set; }
    }
}
