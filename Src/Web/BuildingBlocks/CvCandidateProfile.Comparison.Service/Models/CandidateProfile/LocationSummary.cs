﻿namespace CvCandidateProfile.Comparison.Service.Models.CandidateProfile
{
    public class LocationSummary
    {
        public string Municipality { get; set; }

        public string CountryCode { get; set; }
    }
}
