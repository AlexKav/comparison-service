﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models
{
    public class Stage
    {
        public string PropertyName { get; set; }

        public StageStatus StageStatus { get; set; }
    }
}
