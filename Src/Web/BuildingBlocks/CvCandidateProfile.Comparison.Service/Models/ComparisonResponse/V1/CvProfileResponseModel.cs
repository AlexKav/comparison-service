﻿using System.Collections.Generic;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1
{
    public class CvProfileResponseModel
    {
        public IReadOnlyCollection<ProfileField> ProfileFields { get; set; }
    }
}
