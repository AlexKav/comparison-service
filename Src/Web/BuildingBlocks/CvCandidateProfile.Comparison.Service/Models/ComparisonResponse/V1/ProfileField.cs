using CvCandidateProfile.Comparison.Service.Comparers.Helpers;
using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1
{
    public class ProfileField
    {
        public string ProfileFieldName { get; set; }

        public string ProfileFieldContents { get; set; }

        public string JsonFieldName { get; set; }

        public string JsonFieldContent { get; set; }

        public ProfileSourcesStatus ComparedFieldsFound =>
            ProfileSourcesStatusHelper.GetProfileSoursesStatus(ProfileFieldContents, JsonFieldContent);

        public ComparisonResult ComparisonResult =>
            ComparisonResultHelper.Compare(ProfileFieldContents, JsonFieldContent);
    }
}
