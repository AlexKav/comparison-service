﻿using System.Collections.Generic;

using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class LanguageStatus
    {
        public StageStatus? StageStatus { get; set; }

        public IReadOnlyCollection<LanguageField> ProfileField { get; set; }
    }
}
