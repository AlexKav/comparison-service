﻿using System;

using CvCandidateProfile.Comparison.Service.Comparers.Helpers;
using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class ProfileFieldV2 : IEquatable<ProfileFieldV2>
    {
        public string ProfileFieldName { get; set; }

        public string ProfileFieldContents { get; set; }

        public string JsonFieldName { get; set; }

        public string JsonFieldContent { get; set; }

        public ProfileSourcesStatus ComparedFieldsFound =>
            ProfileSourcesStatusHelper.GetProfileSoursesStatus(ProfileFieldContents, JsonFieldContent, JsonFieldName);

        public ComparisonResult ComparisonResult =>
            ComparisonResultHelper.Compare(ProfileFieldContents, JsonFieldContent, JsonFieldName);

        public bool Equals(ProfileFieldV2 other)
        {
            if (other is null)
            {
                return false;
            }

            return ProfileFieldContents == other.ProfileFieldContents && JsonFieldContent == other.JsonFieldContent;
        }

        public override bool Equals(object obj) => Equals(obj as ProfileFieldV2);

        public override int GetHashCode() => (ProfileFieldContents, JsonFieldContent).GetHashCode();
    }
}
