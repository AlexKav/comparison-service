﻿namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class CvProfileResponseModelV2
    {
        public ProfileFieldStatus Forename { get; set; }

        public ProfileFieldStatus Surname { get; set; }

        public ProfileFieldStatus Gender { get; set; }

        public ProfileFieldStatus Title { get; set; }

        public ProfileFieldStatus PersonalStatement { get; set; }

        public ProfileFieldStatus IsGraduate { get; set; }

        public WorkHistoryStatus UserWorkHistory { get; set; }

        public LanguageStatus Languages { get; set; }
    }
}
