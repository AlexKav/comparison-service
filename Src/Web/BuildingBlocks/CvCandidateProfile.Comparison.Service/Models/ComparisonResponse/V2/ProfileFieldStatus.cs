﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class ProfileFieldStatus
    {
        public StageStatus? StageStatus { get; set; }

        public ProfileFieldV2 ProfileField { get; set; }
    }
}
