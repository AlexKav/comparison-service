﻿using System.Collections.Generic;

using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class WorkHistoryStatus
    {
        public StageStatus? StageStatus { get; set; }

        public IEnumerable<WorkHistory> ProfileField { get; set; }
    }
}
