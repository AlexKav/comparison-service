﻿namespace CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2
{
    public class WorkHistory
    {
        public ProfileFieldV2 DateFrom { get; set; }

        public ProfileFieldV2 DateTo { get; set; }

        public ProfileFieldV2 CompanyName { get; set; }

        public ProfileFieldV2 JobTitle { get; set; }

        public ProfileFieldV2 JobDescription { get; set; }
    }
}
