﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class CvLanguages
    {
        public bool Speak { get; set; }

        public LanguageFluency Proficiency { get; set; }

        public string LanguageCode { get; set; }

        public bool Write { get; set; }

        public bool Read { get; set; }
    }
}
