﻿using System.Collections.Generic;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class EmploymentHistory
    {
        public string EndDate { get; set; }

        public IEnumerable<string> Title { get; set; }

        public string OrgName { get; set; }

        public string Description { get; set; }

        public string StartDate { get; set; }
    }
}
