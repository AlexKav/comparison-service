﻿namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class LocationSummary
    {
        public string Municipality { get; set; }

        public string CountryCode { get; set; }
    }
}
