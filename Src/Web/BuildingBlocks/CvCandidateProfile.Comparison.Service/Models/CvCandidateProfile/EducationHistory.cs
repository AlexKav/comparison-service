﻿using Newtonsoft.Json;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class EducationHistory
    {
        public string EndDate { get; set; }

        public string Comments { get; set; }

        public LocationSummary LocationSummary { get; set; }

        [JsonProperty("schoolType")]
        public string SchoolType { get; set; }

        public Degree Degree { get; set; }

        public string StartDate { get; set; }

        public string SchoolName { get; set; }
    }
}
