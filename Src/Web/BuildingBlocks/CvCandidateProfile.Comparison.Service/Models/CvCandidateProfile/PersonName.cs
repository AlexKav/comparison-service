﻿using CvCandidateProfile.Comparison.Service.Enums;
using Newtonsoft.Json;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class PersonName
    {
        public string FamilyName { get; set; }

        public string GivenName { get; set; }

        [JsonProperty("sex")]
        public CvProfileGender Gender { get; set; }
    }
}
