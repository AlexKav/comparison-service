﻿using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class StructuredResume
    {
        public PersonName PersonName { get; set; }

        [JsonProperty("ExecutiveSummary")]
        public string PersonalStatement { get; set; }

        public IEnumerable<Competency> Competency { get; set; }

        public IEnumerable<EmploymentHistory> EmploymentHistory { get; set; }

        public IEnumerable<EducationHistory> EducationHistory { get; set; }

        [JsonProperty("InternetWebAddress")]
        public IEnumerable<InternetWebAddress> InternetWebAddresses { get; set; }

        public IEnumerable<CvLanguages> Languages { get; set; }

        [JsonIgnore]
        public string JobTitle
        {
            get
            {
                if (EmploymentHistory != null && EmploymentHistory.Any())
                {
                    var presentJobs = EmploymentHistory.Where(job => job.EndDate?.ToLowerInvariant() == "present");
                    if (!presentJobs.Any())
                    {
                        return null;
                    }

                    var titles = presentJobs.Where(job => job.Title != null).SelectMany(job => job.Title).Distinct();
                    var enumerable = titles as string[] ?? titles.ToArray();
                    if (enumerable.Any())
                    {
                        return string.Join(",", enumerable);
                    }
                }

                return null;
            }
        }

        [JsonIgnore]
        public string IsGraduate
        {
            get
            {
                List<string> allowDegrees = new List<string>
                {
                    "some post-graduate",
                    "masters",
                    "doctorate",
                    "postdoctorate",
                    "bachelors"
                };
                if (EducationHistory != null && EducationHistory.Any())
                {
                    var degrees = EducationHistory.Where(x => x.Degree?.DegreeType != null).Select(x => x.Degree?.DegreeType).Distinct();
                    if (!degrees.Any())
                    {
                        return null;
                    }

                    return degrees.Any(x => allowDegrees.Contains(x)) ? "1" : "0";
                }

                return null;
            }
        }
    }
}
