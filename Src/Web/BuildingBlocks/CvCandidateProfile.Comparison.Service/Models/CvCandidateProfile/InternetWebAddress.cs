﻿namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class InternetWebAddress
    {
        public string Content { get; set; }

        public string Type { get; set; }
    }
}
