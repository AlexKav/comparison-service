﻿namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class SkillUsed
    {
        public string Type { get; set; }

        public string Value { get; set; }
    }
}
