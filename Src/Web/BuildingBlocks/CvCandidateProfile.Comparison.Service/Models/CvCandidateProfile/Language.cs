﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class Language
    {
        public string LanguageId { get; set; }

        public LanguageProfileFluency? Fluency { get; set; }
    }
}
