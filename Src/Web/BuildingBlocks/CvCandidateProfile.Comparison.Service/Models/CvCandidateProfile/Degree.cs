﻿namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class Degree
    {
        public string DegreeType { get; set; }

        public string DegreeDate { get; set; }
    }
}
