﻿namespace CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile
{
    public class Competency
    {
        public SkillUsed SkillUsed { get; set; }

        public bool Auth { get; set; }

        public int SkillLevel { get; set; }

        public string SkillName { get; set; }

        public string LastUsed { get; set; }

        public string Description { get; set; }

        public string SkillProficiency { get; set; }
    }
}
