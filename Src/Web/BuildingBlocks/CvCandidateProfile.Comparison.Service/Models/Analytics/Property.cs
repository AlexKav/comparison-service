﻿using System.Collections.Generic;

namespace CvCandidateProfile.Comparison.Service.Models.Analytics
{
    public class Property
    {
        public string Name { get; set; }

        public IList<PropertyValue> Values { get; set; }
    }
}
