﻿using CvCandidateProfile.Comparison.Service.Enums;

namespace CvCandidateProfile.Comparison.Service.Models.Analytics
{
    public class PropertyValue
    {
        public string ProfileContent { get; set; }

        public string CvContent { get; set; }

        public string ReviewedContent { get; set; }

        public StageStatus UpdateStatus { get; set; }

        public ReviewStatus ReviewStatus { get; set; }
    }
}
