﻿using System;
using System.Collections.Generic;

namespace CvCandidateProfile.Comparison.Service.Models.Analytics
{
    public class Snapshot
    {
        public int DaxtraId { get; set; }

        public int CandidateId { get; set; }

        public DateTime Timestamp { get; set; }

        public IList<Property> Properties { get; set; }
    }
}
