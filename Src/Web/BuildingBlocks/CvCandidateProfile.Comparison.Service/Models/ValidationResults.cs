﻿using System.Collections.Generic;

namespace CvCandidateProfile.Comparison.Service.Models
{
    public class ValidationResults
    {
        public IReadOnlyCollection<Stage> ComparisonStages { get; set; }
    }
}
