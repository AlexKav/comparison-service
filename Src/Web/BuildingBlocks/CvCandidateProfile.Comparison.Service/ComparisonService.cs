﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using CvCandidateProfile.Comparison.Service.Comparers;
using CvCandidateProfile.Comparison.Service.Enums;
using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.Comparison.Service.Models;
using CvCandidateProfile.Comparison.Service.Models.CandidateProfile;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V1;
using CvCandidateProfile.Comparison.Service.Models.ComparisonResponse.V2;
using CvCandidateProfile.Comparison.Service.Models.CvCandidateProfile;
using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Newtonsoft.Json;

namespace CvCandidateProfile.Comparison.Service
{
    public class ComparisonService : IComparisonService
    {
        private readonly IPostgreSqlService _postgreSqlService;
        private readonly ICandidateProfileService _candidateProfileService;
        private readonly ILanguageFactory _languageFactory;
        private readonly List<string> _fieldsName = new List<string>()
        {
            "Forename",
            "Surname",
            "JobTitle",
            "PersonalStatement",
            "IsGraduate",
            "Email",
            "Postcode",
            "Country",
            "Town",
            "PhoneNumber",
            "UserWorkHistory",
            "Education",
            "Languages",
            "UserSkills",
            "SocialMedia",
            "DrivingLicense"
        };

        public ComparisonService(IPostgreSqlService postgreSqlService, ICandidateProfileService candidateProfileService, ILanguageFactory languageFactory)
        {
            _postgreSqlService = postgreSqlService ?? throw new ArgumentNullException(nameof(postgreSqlService));
            _candidateProfileService = candidateProfileService ?? throw new ArgumentNullException(nameof(postgreSqlService));
            _languageFactory = languageFactory ?? throw new ArgumentNullException(nameof(languageFactory));
        }

        public async Task<CvProfileResponseModel> CompareProfilesAsync(int candidateId)
        {
            JobseekerProfileResult candidateProfile = await GetCandidateProfileAsync(candidateId);
            if (candidateProfile == null)
            {
                throw new ValidationException($"Candidate profile not found matching {candidateId}");
            }

            DaxtraProfileResult cvCandidateProfile = await GetCvCandidateProfileAsync(candidateId);
            if (cvCandidateProfile == null)
            {
                throw new ValidationException($"Candidate {candidateId} parsed JSON data not found");
            }

            CvProfileResponseModel result = FillCvProfileResponseModel(candidateProfile, cvCandidateProfile);

            return result;
        }

        public CvProfileResponseModel CompareProfiles(int candidateId, string candidateProfileJson)
        {
            JobseekerProfileResult candidateProfile = GetCandidateProfile(candidateProfileJson);
            if (candidateProfile == null)
            {
                throw new ValidationException($"Candidate profile not found matching {candidateId}");
            }

            DaxtraProfileResult cvCandidateProfile = GetCvCandidateProfile(candidateId);
            if (cvCandidateProfile == null)
            {
                throw new ValidationException($"Candidate {candidateId} parsed JSON data not found");
            }

            CvProfileResponseModel result = FillCvProfileResponseModel(candidateProfile, cvCandidateProfile);

            return result;
        }

        public async Task<CvProfileResponseModelV2> CompareProfilesV2Async(int candidateId, string candidateProfileJson)
        {
            JobseekerProfileResult candidateProfile = GetCandidateProfile(candidateProfileJson);
            if (candidateProfile == null)
            {
                throw new ValidationException($"Candidate profile not found matching {candidateId}");
            }

            DaxtraProfileResult cvCandidateProfile = await GetCvCandidateProfileAsync(candidateId);
            if (cvCandidateProfile == null)
            {
                throw new ValidationException($"Candidate {candidateId} parsed JSON data not found");
            }

            var stagesResult = GetCandidateProfileValidationResults(candidateId)?.ComparisonStages;
            CvProfileResponseModelV2 result = FillCvProfileResponseModel_V2(candidateProfile, cvCandidateProfile, stagesResult);

            return result;
        }

        public bool UpdateProfileReviewStages(int candidateId, int daxtraStoreId, IEnumerable<Stage> propertyStageStatuses)
        {
            if (candidateId == 0)
            {
                throw new ValidationException($"Data from request CandidateId={candidateId} is not valid");
            }

            if (daxtraStoreId == 0)
            {
                throw new ValidationException($"Data from request DaxtraStoreId={daxtraStoreId} is not valid");
            }

            var reviewStages = propertyStageStatuses as IList<Stage> ?? propertyStageStatuses?.ToList();
            if (reviewStages == null || !reviewStages.Any())
            {
                throw new ValidationException($"Data from request PropertyStageStatuses is not valid");
            }

            var profileAttributes = GetCandidateProfileValidationResults(candidateId, daxtraStoreId) ?? new ValidationResults();
            var stageStatuses = profileAttributes.ComparisonStages?.ToList() ?? GetProfileReviewStagesFullList().ToList();
            var missedProperties = _fieldsName.Where(x => stageStatuses.All(y => y.PropertyName != x));

            stageStatuses.AddRange(missedProperties.Select(name =>
                new Stage
                {
                    PropertyName = name,
                    StageStatus = StageStatus.NotReviewed
                }));

            foreach (var reviewStage in reviewStages)
            {
                var stage = stageStatuses.FirstOrDefault(x =>
                    x.PropertyName == reviewStage.PropertyName);

                if (stage != null)
                {
                    stage.StageStatus = reviewStage.StageStatus;
                }
            }
            profileAttributes.ComparisonStages = stageStatuses;

            return UpdateStages(daxtraStoreId, profileAttributes);
        }

        private IList<Stage> GetProfileReviewStagesFullList()
        {
            return _fieldsName.Select(name => new Stage()
            {
                PropertyName = name,
                StageStatus = StageStatus.NotReviewed
            }).ToList();
        }

        private bool UpdateStages(int daxtraStoreId, ValidationResults profileAttributes)
        {
            string jsonStage = JsonConvert.SerializeObject(profileAttributes);
            var responseContent = _postgreSqlService.UpdateProfileReviewStages(daxtraStoreId, jsonStage);
            return responseContent;
        }

        private async Task<DaxtraProfileResult> GetCvCandidateProfileAsync(int candidateId)
        {
            var responseContent = await _postgreSqlService.ReadDaxtraByIdAsync(candidateId);
            if (string.IsNullOrEmpty(responseContent))
            {
                return null;
            }

            var model = JsonConvert.DeserializeObject<DaxtraProfileResult>(responseContent);
            return model;
        }

        private async Task<JobseekerProfileResult> GetCandidateProfileAsync(int candidateId)
        {
            var responseContent = await _candidateProfileService.GetJsonCandidateProfileAsync(candidateId);
            if (string.IsNullOrWhiteSpace(responseContent))
            {
                return null;
            }

            var model = JsonConvert.DeserializeObject<JobseekerProfileResult>(responseContent);
            return model;
        }

        private static JobseekerProfileResult GetCandidateProfile(string candidateProfileJson)
        {
            if (string.IsNullOrWhiteSpace(candidateProfileJson))
            {
                return null;
            }

            var model = JsonConvert.DeserializeObject<JobseekerProfileResult>(candidateProfileJson);
            return model;
        }

        private DaxtraProfileResult GetCvCandidateProfile(int candidateId)
        {
            var responseContent = _postgreSqlService.ReadDaxtraById(candidateId);
            if (string.IsNullOrEmpty(responseContent))
            {
                return null;
            }

            var model = JsonConvert.DeserializeObject<DaxtraProfileResult>(responseContent);
            return model;
        }

        private static CvProfileResponseModel FillCvProfileResponseModel(JobseekerProfileResult candidateProfile, DaxtraProfileResult cvCandidateProfile)
        {
            var profileFields = new List<ProfileField>
            {
                new ProfileField
                {
                    JsonFieldName = "GivenName",
                    JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonName?.GivenName,
                    ProfileFieldName = "Forename",
                    ProfileFieldContents = candidateProfile?.Profile?.Forename,
                },
                new ProfileField
                {
                    JsonFieldName = "FamilyName",
                    JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonName?.FamilyName,
                    ProfileFieldName = "Surname",
                    ProfileFieldContents = candidateProfile?.Profile?.Surname,
                },
                new ProfileField
                {
                    JsonFieldName = "JobTitle",
                    JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.JobTitle,
                    ProfileFieldName = "JobTitle",
                    ProfileFieldContents = candidateProfile?.Profile?.JobTitle,
                },
                new ProfileField
                {
                    JsonFieldName = "PersonalStatement",
                    JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonalStatement,
                    ProfileFieldName = "PersonalStatement",
                    ProfileFieldContents = candidateProfile?.Profile?.PersonalStatement,
                },
                new ProfileField
                {
                    JsonFieldName = "IsGraduate",
                    JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.IsGraduate,
                    ProfileFieldName = "IsGraduate",
                    ProfileFieldContents = candidateProfile?.Profile?.IsGraduate,
                }
            };

            CvProfileResponseModel cvProfileResponseModel = new CvProfileResponseModel { ProfileFields = profileFields };

            return cvProfileResponseModel;
        }

        private CvProfileResponseModelV2 FillCvProfileResponseModel_V2(
            JobseekerProfileResult candidateProfile,
            DaxtraProfileResult cvCandidateProfile,
            IReadOnlyCollection<Stage> stagesResult = null)
        {
            var cvProfileResponseModel = new CvProfileResponseModelV2
            {
                Forename = new ProfileFieldStatus
                {
                    ProfileField = new ProfileFieldV2
                    {
                        ProfileFieldName = "Forename",
                        ProfileFieldContents = candidateProfile?.Profile?.Forename,
                        JsonFieldName = "GivenName",
                        JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonName?.GivenName
                    },
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "Forename")?.StageStatus
                },
                Surname = new ProfileFieldStatus
                {
                    ProfileField = new ProfileFieldV2
                    {
                        ProfileFieldName = "Surname",
                        ProfileFieldContents = candidateProfile?.Profile?.Surname,
                        JsonFieldName = "FamilyName",
                        JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonName?.FamilyName
                    },
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "Surname")?.StageStatus
                },
                Title = new ProfileFieldStatus
                {
                    ProfileField = new ProfileFieldV2
                    {
                        ProfileFieldName = "JobTitle",
                        ProfileFieldContents = candidateProfile?.Profile?.JobTitle,
                        JsonFieldName = "JobTitle",
                        JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.JobTitle
                    },
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "JobTitle")?.StageStatus
                },
                PersonalStatement = new ProfileFieldStatus
                {
                    ProfileField = new ProfileFieldV2
                    {
                        ProfileFieldName = "PersonalStatement",
                        ProfileFieldContents = candidateProfile?.Profile?.PersonalStatement,
                        JsonFieldName = "PersonalStatement",
                        JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.PersonalStatement
                    },
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "PersonalStatement")?.StageStatus
                },
                IsGraduate = new ProfileFieldStatus
                {
                    ProfileField = new ProfileFieldV2
                    {
                        ProfileFieldName = "IsGraduate",
                        ProfileFieldContents = candidateProfile?.Profile?.IsGraduate,
                        JsonFieldName = "IsGraduate",
                        JsonFieldContent = cvCandidateProfile?.Resume?.StructuredResume?.IsGraduate
                    },
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "IsGraduate")?.StageStatus
                },
                UserWorkHistory = new WorkHistoryStatus
                {
                    ProfileField = FillWorkHistory(candidateProfile, cvCandidateProfile),
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "UserWorkHistory")?.StageStatus
                },
                Languages = new LanguageStatus()
                {
                    ProfileField = FillLanguages(candidateProfile, cvCandidateProfile).ToList(),
                    StageStatus = stagesResult?.FirstOrDefault(x => x.PropertyName == "Languages")?.StageStatus
                }
            };

            return cvProfileResponseModel;
        }

        private static IEnumerable<WorkHistory> FillWorkHistory(JobseekerProfileResult candidateProfile, DaxtraProfileResult cvCandidateProfile)
        {
            var cvEmploymentHistoryList = cvCandidateProfile.Resume.StructuredResume.EmploymentHistory?
                .OrderByDescending(x => x.StartDate).ToList();

            var jobSeekerWorkHistoryList = candidateProfile.Profile.WorkExperience?
                .OrderByDescending(x => x.StartDate).ToList();

            var userWorkHistory = new List<WorkHistory>();
            if (cvEmploymentHistoryList != null && cvEmploymentHistoryList.Any())
            {
                foreach (EmploymentHistory cvEmploymentHistory in cvEmploymentHistoryList)
                {
                    bool jobSeekerWorkHistoryFilled = false;

                    if (jobSeekerWorkHistoryList != null && jobSeekerWorkHistoryList.Any())
                    {
                        foreach (WorkExperience jobSeekerWorkHistory in jobSeekerWorkHistoryList)
                        {
                            if (jobSeekerWorkHistory.Company == cvEmploymentHistory.OrgName &&
                                    (CompareDates(jobSeekerWorkHistory.StartDate, cvEmploymentHistory.StartDate) ||
                                    CompareTitles(cvEmploymentHistory.Title, jobSeekerWorkHistory.JobTitle, cvEmploymentHistory.StartDate)))
                            {
                                FillWorkHistorySingleLine(userWorkHistory, cvEmploymentHistory, jobSeekerWorkHistory);

                                jobSeekerWorkHistoryFilled = true;

                                if (jobSeekerWorkHistoryList.Any())
                                {
                                    jobSeekerWorkHistoryList.Remove(jobSeekerWorkHistory);
                                }

                                break;
                            }
                        }
                    }

                    if (!jobSeekerWorkHistoryFilled)
                    {
                        FillWorkHistorySingleLine(userWorkHistory, cvEmploymentHistory, null);
                    }
                }

                if (jobSeekerWorkHistoryList != null && jobSeekerWorkHistoryList.Any())
                {
                    foreach (WorkExperience jobSeekerWorkHistory in jobSeekerWorkHistoryList)
                    {
                        FillWorkHistorySingleLine(userWorkHistory, null, jobSeekerWorkHistory);
                    }
                }
            }
            else
            {
                if (jobSeekerWorkHistoryList != null && jobSeekerWorkHistoryList.Any())
                {
                    foreach (WorkExperience jobSeekerWorkHistory in jobSeekerWorkHistoryList)
                    {
                        FillWorkHistorySingleLine(userWorkHistory, null, jobSeekerWorkHistory);
                    }
                }
                else
                {
                    return Enumerable.Empty<WorkHistory>();
                }
            }

            return userWorkHistory.OrderByDescending(x => x.DateFrom.JsonFieldContent ??
                                                          x.DateFrom.ProfileFieldContents).ToList();
        }

        private static bool CompareDates(DateTime? jobSeekerStartDate, string cvEmploymentStartDate)
        {
            var comparer = new DateToComparer();

            return comparer.Compare(jobSeekerStartDate?.ToString("yyyy-MM"), cvEmploymentStartDate) == ComparisonResult.Match;
        }

        private static bool CompareTitles(IEnumerable<string> cvEmploymentTitle, string jobSeekerTitle, string cvEmploymentStartDate)
        {
            return string.IsNullOrWhiteSpace(cvEmploymentStartDate) &&
                   cvEmploymentTitle.Contains(jobSeekerTitle);
        }

        private static void FillWorkHistorySingleLine(
            ICollection<WorkHistory> userWorkHistory,
            EmploymentHistory cvEmploymentHistory,
            WorkExperience jobSeekerWorkExperience)
        {
            userWorkHistory.Add(new WorkHistory
            {
                DateFrom = new ProfileFieldV2
                {
                    ProfileFieldName = "StartDate",
                    ProfileFieldContents = jobSeekerWorkExperience?.StartDate.ToString("yyyy-MM"),
                    JsonFieldName = "StartDate",
                    JsonFieldContent = cvEmploymentHistory?.StartDate
                },
                DateTo = new ProfileFieldV2
                {
                    ProfileFieldName = "EndDate",
                    ProfileFieldContents = jobSeekerWorkExperience?.EndDate?.ToString("yyyy-MM"),
                    JsonFieldName = "EndDate",
                    JsonFieldContent = cvEmploymentHistory?.EndDate
                },
                CompanyName = new ProfileFieldV2
                {
                    ProfileFieldName = "Company",
                    ProfileFieldContents = jobSeekerWorkExperience?.Company,
                    JsonFieldName = "OrgName",
                    JsonFieldContent = cvEmploymentHistory?.OrgName
                },
                JobTitle = new ProfileFieldV2
                {
                    ProfileFieldName = "JobTitle",
                    ProfileFieldContents = jobSeekerWorkExperience?.JobTitle == null ? null : string.Join(",", jobSeekerWorkExperience.JobTitle),
                    JsonFieldName = "Title",
                    JsonFieldContent = cvEmploymentHistory?.Title == null ? null : string.Join(",", cvEmploymentHistory.Title)
                },
                JobDescription = new ProfileFieldV2
                {
                    ProfileFieldName = "Responsibilities",
                    ProfileFieldContents = jobSeekerWorkExperience?.Responsibilities,
                    JsonFieldName = "Description",
                    JsonFieldContent = cvEmploymentHistory?.Description
                }
            });
        }

        private ValidationResults GetCandidateProfileValidationResults(int candidateId, int? daxtraStoreId = null)
        {
            var responseContent = _postgreSqlService.GetCandidateProfileValidationResults(candidateId, daxtraStoreId);

            try
            {
                var model = JsonConvert.DeserializeObject<ValidationResults>(responseContent);
                return model;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool IsCandidateProfileReviewFinished(int candidateId)
        {
            var profileAttributes = GetCandidateProfileValidationResults(candidateId, null) ?? new ValidationResults();
            var stageStatuses = profileAttributes.ComparisonStages;
            if (stageStatuses == null)
            {
                return false;
            }

            bool isReviewed = true;
            stageStatuses.ToList().ForEach(x => isReviewed &= x.StageStatus == StageStatus.Reviewed || x.StageStatus == StageStatus.NoReviewNeeded);

            return isReviewed;
        }

        private IEnumerable<LanguageField> FillLanguages(JobseekerProfileResult candidateProfile, DaxtraProfileResult cvCandidateProfile)
        {
            var convertedLanguagesCv =
                ConvertCvLanguagesToProfile(cvCandidateProfile?.Resume?.StructuredResume?.Languages)?.ToList() ?? Enumerable.Empty<Language>().ToList();

            var resultProfileLanguages = candidateProfile?.Profile?.Languages?.Select(x => _languageFactory.CreateLanguageFieldFromProfile(convertedLanguagesCv, x)).ToList();
            if (resultProfileLanguages == null)
            {
                return convertedLanguagesCv.Select(_languageFactory.CreateLanguageFieldFromCvWithEmptyProfile()).ToList();
            }
            var resultCvLanguage = convertedLanguagesCv.Select(
                x => _languageFactory.CreateLanguageFieldFromCv(x, resultProfileLanguages)).ToList();

            return resultProfileLanguages.Union(resultCvLanguage);
        }

        private static LanguageProfileFluency GetProfileFluency(LanguageFluency languageFluency)
        {
            switch (languageFluency)
            {
                case LanguageFluency.Native:
                case LanguageFluency.Excellent:
                case LanguageFluency.Fluent:
                    return LanguageProfileFluency.Fluent;

                case LanguageFluency.Advanced:
                case LanguageFluency.Intermediate:
                    return LanguageProfileFluency.Intermediate;

                case LanguageFluency.Basic:
                    return LanguageProfileFluency.Basic;

                default:
                    return LanguageProfileFluency.None;
            }
        }

        private IEnumerable<Language> ConvertCvLanguagesToProfile(IEnumerable<CvLanguages> cvLanguages)
        {
            return cvLanguages?.Select(x => new Language
            {
                LanguageId = _postgreSqlService.GetLanguageIdByLanguageCode(x.LanguageCode).ToString(),
                Fluency = GetProfileFluency(x.Proficiency)
            });
        }
    }
}
