﻿using CvCandidateProfile.Comparison.Service.Interfaces;
using CvCandidateProfile.PostgreSql.Service;
using CvCandidateProfile.PostgreSql.Service.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CvCandidateProfile.Comparison.Service
{
    public static class CandidateProfileServiceRegistration
    {
        public static void AddCandidateProfileService(this IServiceCollection serviceCollection, string postgreConnectionString)
        {
            serviceCollection.AddSingleton<ICandidateProfileService, CandidateProfileService>();

            serviceCollection.AddSingleton<ILanguageFactory, LanguageFaсtory>();

            serviceCollection.AddSingleton<IDbConnectionFactory>(new DbConnectionFactory(postgreConnectionString));

            serviceCollection.AddTransient<IPostgreSqlService, PostgreSqlService>();

            serviceCollection.AddTransient<IComparisonService, ComparisonService>();
        }
    }
}
